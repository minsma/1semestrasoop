﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_savarankiško_darbo_užduotis
{
    class Program
    {
        static void Main(string[] args)
        {
            char character;
            Console.WriteLine("Įveskite spausdinamą simbolį");
            character = (char)Console.Read();
            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 6; j++)
                    Console.Write(character);
                Console.WriteLine();
            }
            Console.WriteLine("");
            Console.ReadKey();
        }
    }
}
