﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 10 variantas (komp numeris 154)

namespace _2_savarankiško_darbo_užduotis
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iveskite skaiciu: ");
            double x = double.Parse(Console.ReadLine());
            double rez; // bus saugojamas rezultatas
            if (x >= -5 && x < 0)
                rez = 1 / (x * x);
            else if (x >= 0 && x <= 2)
                rez = Math.Sin(x + 1);
            else
                rez = Math.Sqrt(2 * x);
            Console.WriteLine("{0}", rez);
        }
    }
}
