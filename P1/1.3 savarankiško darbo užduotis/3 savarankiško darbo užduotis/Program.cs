﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_savarankiško_darbo_užduotis
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iveskite pirma skaiciu:");
            double fnumber = double.Parse(Console.ReadLine());
            Console.WriteLine("Iveskite operacijos simboli:");
            char simb = char.Parse(Console.ReadLine());
            Console.WriteLine("Iveskite antra skaiciu:");
            double snumber = double.Parse(Console.ReadLine());
            switch (simb)
            {
                case '+':
                    Console.WriteLine("{0} {1} {2} = {3}", fnumber, simb, snumber, fnumber + snumber);
                    break;
                case '-':
                    Console.WriteLine("{0} {1} {2} = {3}", fnumber, simb, snumber, fnumber - snumber);
                    break;
                case '*':
                    Console.WriteLine("{0} {1} {2} = {3}", fnumber, simb, snumber, fnumber * snumber);
                    break;
                case '/':
                    Console.WriteLine("{0} {1} {2} = {3}", fnumber, simb, snumber, fnumber / snumber);
                    break;
                default:
                    Console.WriteLine("ERROR");
                    break;
            }
        }
    }
}
