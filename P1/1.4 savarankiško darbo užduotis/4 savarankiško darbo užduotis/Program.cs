﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_savarankiško_darbo_užduotis
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kuos jus vardu?");
            string name = Console.ReadLine();
            if (name[name.Length - 1] == 's') {
                if (name[name.Length - 2] == 'a')
                {
                    name = name.Remove(name.Length - 1);
                    name += 'i';
                }
                else
                    if (name[name.Length - 2] == 'i')
                {
                    name = name.Remove(name.Length - 1);
                }
                else
                    if (name[name.Length - 2] == 'y')
                {
                    name = name.Remove(name.Length - 2);
                    name += 'į';
                }
            }
            else
            {
                if (name[name.Length - 1] == 'ė')
                {
                    name = name.Remove(name.Length - 1);
                    name += 'e';
                }
            }

            Console.WriteLine("Labas, {0}", name);            
        }
    }
}
