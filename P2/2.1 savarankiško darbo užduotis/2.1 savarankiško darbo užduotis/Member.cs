﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2._1_savarankiško_darbo_užduotis
{
    class Member
    {
        public string Name { get; set; }
        public int Eur { get; set; }
        public int Cent { get; set; }
        public int EurCent { get; set; }
        public int Quater { get; set; }

        public Member()
        {
        }

        public Member(string name, int eur, int cent)
        {
            Name = name;
            Eur = eur;
            Cent = cent;
        }
    }
}
