﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2._1_savarankiško_darbo_užduotis
{
    class Program
    {
        public const int MaxNumberOfMembers = 50;

        static void Main(string[] args)
        {
            Member[] members;
            int memberCount = 0;
            ReadDataMember(out members, out memberCount);
            MoneyToCents(members, memberCount);
            MoneyQuater(members, memberCount);
            int totalMoney = TotalMoneyForExpenses(members, memberCount);
            int mostExpenses = MostExpenses(members, memberCount);
            Member[] mostExpensesMembers;
            int mostExpensesCount = 0;
            MostExpensesMembers(members, memberCount, mostExpenses, out mostExpensesMembers, out mostExpensesCount);
            PrintToConsole(mostExpenses, mostExpensesMembers, mostExpensesCount, totalMoney);
            PrintToFile(mostExpenses, mostExpensesMembers, mostExpensesCount, totalMoney);
        }
        /// <summary>
        /// Skaito Member duomenis is CSV failo
        /// </summary>
        /// <param name="members">Grazina members masyva</param>
        /// <param name="count">Grazina members kieki</param>
        static private void ReadDataMember(out Member[] members, out int memberCount)
        {
            memberCount = 0;
            members = new Member[MaxNumberOfMembers];

            using (StreamReader reader = new StreamReader(@"Data.csv"))
            {
                string line = null;
                while(null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(';');
                    string name = values[0];
                    int eur = int.Parse(values[1]);
                    int cent = int.Parse(values[2]);
                    Member member = new Member(name, eur, cent);

                    members[memberCount++] = member; 
                }
            }
        }
        /// <summary>
        /// Pavercia members turimus pinigus eurais ir euro centais i euro centus
        /// </summary>
        /// <param name="members">Member masyvas</param>
        /// <param name="memberCount">members kiekis</param>
        static private void MoneyToCents(Member[] members, int memberCount)
        {
            for (int i = 0; i < memberCount; i++)
            {
                members[i].EurCent = members[i].Eur * 100 + members[i].Cent;
            }
        }
        /// <summary>
        /// Pavercia members turimus pinigus euro centais i ketvircius
        /// </summary>
        /// <param name="members">Member masyvas</param>
        /// <param name="memberCount">members kiekis</param>
        static private void MoneyQuater(Member[] members, int memberCount)
        {
            for (int i = 0; i < memberCount; i++)
            {
                members[i].Quater = members[i].EurCent / 4;
            }
        }
        /// <summary>
        /// Grazina bendra islaidoms skirta suma
        /// </summary>
        /// <param name="members">Member masyvas</param>
        /// <param name="memberCount">members kiekis</param>
        static private int TotalMoneyForExpenses(Member[] members, int memberCount)
        {
            int totalMoneyForexpenses = 0;
            for (int i = 0; i < memberCount; i++)
            {
                totalMoneyForexpenses += members[i].Quater;
            }
            return totalMoneyForexpenses;
        }
        /// <summary>
        /// Randa didziausia islaidoms skirta suma
        /// </summary>
        /// <param name="members">Member masyvas</param>
        /// <param name="memberCount">members kiekis</param>
        static private int MostExpenses(Member[] members, int memberCount)
        {
            int biggest = 0;
            for (int i = 0; i < memberCount; i++)
            {
                if (members[i].Quater > biggest)
                {
                    biggest = members[i].Quater;
                }
            }
            return biggest;
        }
        /// <summary>
        /// Randa asmenis kurie skyre didziausia suma islaidoms ir sudeda juos i nauja masyva, kuris yra grazinamas i main funckija kartu su ju kiekiu
        /// </summary>
        /// <param name="members">Member masyvas</param>
        /// <param name="memberCount">members kiekis</param>
        /// <param name="MostExpenses">didziausia islaidoms skirta suma</param>
        /// <param name="mostExpensesMembers">didziausia islaidoms skirta suma skyrusiu nariu masyvas</param>
        /// <param name="mostExpensesCount">didziausia islaidoms skirta suma skyrusiu nariu kiekis</param>
        static private void MostExpensesMembers(Member[] members, int memberCount, int MostExpenses, out Member[] mostExpensesMembers, out int mostExpensesCount)
        {
            mostExpensesMembers = new Member[MaxNumberOfMembers];
            mostExpensesCount = 0;

            for (int i = 0; i < memberCount; i++)
            {
                if (members[i].Quater == MostExpenses)
                {
                    mostExpensesMembers[mostExpensesCount++] = members[i];
                }
            }
        }
        /// <summary>
        /// Atspausdina rezultatus i console
        /// </summary>
        /// <param name="mostExpenses">Didziausia islaidoms vieno nario skirta suma</param>
        /// <param name="mostExpensesMembers">Didziausia islaidoms skirta suma skyrusiu nariu masyvas</param>
        /// <param name="mostExpensesCount">Didziausia islaidoms skirta suma skyrusiu nariu kiekis</param>
        /// <param name="totalMoney">Bendra visu nariu islaidoms skirta suma</param>
        static private void PrintToConsole(int mostExpenses, Member[] mostExpensesMembers, int mostExpensesCount, int totalMoney)
        {
            Console.WriteLine("Bendrai islaidoms skirta suma: {0} eurai ir {1} euro centai", CentsToEuros(totalMoney), CentsWithoutEuros(totalMoney));
            Console.WriteLine("Didziausia islaidoms skirta : {0} eurai ir {1} euro centai",  CentsToEuros(mostExpenses), CentsWithoutEuros(mostExpenses));
            Console.WriteLine("Daugiausiai islaidoms pinigu skyre nariai:");
            for (int i = 0; i < mostExpensesCount; i++)
            {
                Console.WriteLine(mostExpensesMembers[i].Name);
            }
        }
        /// <summary>
        /// Spausdina rezultatus i CSV faila
        /// </summary>
        /// <param name="mostExpenses">Didziausia islaidoms vieno nario skirta suma</param>
        /// <param name="mostExpensesMembers">Didziausia islaidoms skirta suma skyrusiu nariu masyvas</param>
        /// <param name="mostExpensesCount">Didziausia islaidoms skirta suma skyrusiu nariu kiekis</param>
        /// <param name="totalMoney">Bendra visu nariu islaidoms skirta suma</param>
        static private void PrintToFile(int mostExpenses, Member[] mostExpensesMembers, int mostExpensesCount, int totalMoney)
        {
            using (StreamWriter writer = new StreamWriter(@"Result.csv"))
            {
                writer.WriteLine("Bendrai islaidoms skirta suma: {0} eurai ir {1} euro centai", CentsToEuros(totalMoney), CentsWithoutEuros(totalMoney));
                writer.WriteLine("Didziausia islaidoms skirta : {0} eurai ir {1} euro centai", CentsToEuros(mostExpenses), CentsWithoutEuros(mostExpenses));
                writer.WriteLine("Daugiausiai islaidoms pinigu skyre nariai:");
                for (int i = 0; i < mostExpensesCount; i++)
                {
                    writer.WriteLine(mostExpensesMembers[i].Name);
                }
            }
        }
        /// <summary>
        /// Konvertuotoja suma euro centais i eurus
        /// </summary>
        /// <param name="eurocents">suma euro centais</param>
        static private int CentsToEuros(int eurocents)
        {
            return eurocents / 100;
        }
        /// <summary>
        /// Is sumos euro centais atemus eurus gaunami centai
        /// </summary>
        /// <param name="eurocents">suma euro centais</param>
        static private int CentsWithoutEuros(int eurocents)
        {
            return eurocents - (CentsToEuros(eurocents) * 100);
        }
    }
}
