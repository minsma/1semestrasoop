﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labarotorinis_darbas
{
    class Program
    {

        const int MaxRingsAmount = 50; // maksimalus ziedu kiekis
        const string CRead1 = "L1-11.csv";
        const string CPrint1 = "BA300.csv";
        const string CPrint2 = "Results.csv";

        static void Main(string[] args)
        {
            Ring[] rings;
            int ringsCounter;
            string[] fline;

            ReadDataFromFile(out rings, out ringsCounter, out fline);

            double biggestPrice = MostExpensiveRingPrice(rings, ringsCounter);

            Ring[] mostExpensiveRings;
            int mostExpensiveCounter;

            MostExpensiveRings(rings, ringsCounter, out mostExpensiveRings, out mostExpensiveCounter, biggestPrice);

            int[] hallmarks;
            int[] hallmarksCounters;
            int hallmarksCounter;

            DifferentHallmarksAndCounters(rings, ringsCounter, out hallmarks, out hallmarksCounters, out hallmarksCounter);

            int mostRecurHallmakNumber = MostRecurHallmark(hallmarks, hallmarksCounter, hallmarksCounters);

            Ring[] cheaperThan300Rings;
            int cheaperThan300RingsCounter;

            CheaperThan300Rings(rings, ringsCounter, out cheaperThan300Rings, out cheaperThan300RingsCounter);

            Ring[] ringsBetweenInterval;
            int ringsBetweenIntervalCounter;

            ThreeRingsBetweenInterval(rings, ringsCounter, out ringsBetweenInterval, out ringsBetweenIntervalCounter);

            PrintCheaperThan300Rings(cheaperThan300Rings, cheaperThan300RingsCounter, fline);

            PrintResultsToFile(rings, ringsCounter, mostExpensiveRings, mostExpensiveCounter, mostRecurHallmakNumber, hallmarks, hallmarksCounters, ringsBetweenInterval, ringsBetweenIntervalCounter, fline);

        }
        /// <summary>
        /// Funkcija skaitanti pradinius duomenis is CSV failo
        /// </summary>
        /// <param name="rings">Ring klases objektu masyvas</param>
        /// <param name="ringsCounter">Ring klases objektu kiekis</param>
        /// <param name="fline">Pirma pradiniu duomenu failo eilute, skirta pradiniu duomenu lenteles formavimui spausdinime</param>
        static private void ReadDataFromFile(out Ring[] rings, out int ringsCounter, out string[] fline)
        {
            rings = new Ring[MaxRingsAmount];
            fline = new string[7];
            ringsCounter = 0;

            using (StreamReader reader = new StreamReader(@CRead1))
            {
                string line = null;
                int counter = 0;
                while (null != (line = reader.ReadLine()))
                {
                    if (counter == 0)
                    {
                        string[] values = line.Split(';');
                        for(int i = 0; i < 7; i++)
                        {
                            fline[i] = values[i];
                        }
                        counter++;
                    }
                    else
                    {
                        string[] values = line.Split(';');
                        string producer = values[0];
                        string name = values[1];
                        string metal = values[2];
                        double weight = Convert.ToDouble(values[3]);
                        double size = Convert.ToDouble(values[4]);
                        int hallmark = Convert.ToInt32(values[5]);
                        double price = Convert.ToDouble(values[6]);
                        Ring ring = new Ring(producer, name, metal, weight, size, hallmark, price);

                        rings[ringsCounter++] = ring;
                    }
                }
            }

        }
        /// <summary>
        /// Funkcija randanti ir grazinanti didziausia ziedo kaina
        /// </summary>
        /// <param name="rings">Ring klases objektu masyvas</param>
        /// <param name="ringsCounter">Ring klases objektu kiekis</param>
        /// <returns>Didziausia ziedo kaina</returns>
        static private double MostExpensiveRingPrice(Ring[] rings, int ringsCounter)
        {
            double biggestPrice = 0;
            for (int i = 0; i < ringsCounter; i++)
            {
                if (rings[i].Price > biggestPrice)
                    biggestPrice = rings[i].Price;
            }
            return biggestPrice;

        }
        /// <summary>
        /// Funkcija surandanti visus ziedus, kuriu kaina lygi didziausiai ir tu zaideju kiekis
        /// </summary>
        /// <param name="rings">Ring klases objektu masyvas</param>
        /// <param name="ringsCounter">Ring klases objektu kiekis</param>
        /// <param name="mostExpensiveRings">Ring klases objektu lygiu didziausiai kainai masyvas</param>
        /// <param name="mostExpensiveCounter">Ring klases objektu lygiu didziausiai kainai kiekis</param>
        /// <param name="biggestPrice">Didziausia Ring klases objekto kaina</param>
        static private void MostExpensiveRings(Ring[] rings, int ringsCounter, out Ring[] mostExpensiveRings, out int mostExpensiveCounter, double biggestPrice)
        {
            mostExpensiveRings = new Ring[MaxRingsAmount];
            mostExpensiveCounter = 0;

            for (int i = 0; i < ringsCounter; i++)
            {
                if (rings[i].Price == biggestPrice)
                {
                    mostExpensiveRings[mostExpensiveCounter++] = rings[i];
                }
            }
        }
        /// <summary>
        /// Funkcija sudaro skirtingu prabu ir ju pasikartojimo Ring klases masyve masyvai ir surandamas ju kiekis
        /// </summary>
        /// <param name="rings">Ring klases objektu masyvas</param>
        /// <param name="ringsCounter">Ring klases objektu kiekis</param>
        /// <param name="hallmarks">Skirtingu prabu masyvas</param>
        /// <param name="hallmarksCounters">Skirtingu prabu pasikartojimo kiekiu masyvas</param>
        /// <param name="hallmarksCounter">Skirtingu prabu kiekis</param>
        static private void DifferentHallmarksAndCounters(Ring[] rings, int ringsCounter, out int[] hallmarks, out int[] hallmarksCounters, out int hallmarksCounter)
        {
            hallmarks = new int[MaxRingsAmount];
            hallmarksCounters = new int[MaxRingsAmount];
            hallmarksCounter = 0;


            for (int i = 0; i < ringsCounter; i++)
            {
                if (DoesThisHallmarkExist(hallmarks, hallmarksCounter, rings[i].Hallmark) == -1)
                {
                    hallmarks[hallmarksCounter] = rings[i].Hallmark;
                    hallmarksCounters[hallmarksCounter]++;
                    hallmarksCounter++;
                }
                else
                {
                    hallmarksCounters[DoesThisHallmarkExist(hallmarks, 
                        hallmarksCounter, rings[i].Hallmark)]++;
                }
            }
        }
        /// <summary>
        /// Funkcija tikrinanti ar pateikta praba jau egzistuoja esamame masyve
        /// </summary>
        /// <param name="hallmarks">Prabu masyvas, kuriame bus ieskoma</param>
        /// <param name="hallmarksCounter">Prabu kiekis masyve</param>
        /// <param name="hallmark">Pateikiama praba ieskojimui masyve, ar ji egzistuoja</param>
        /// <returns>Grazinama vieta masyve, jeigu randama praba masyve, o jeigu nerandama -1</returns>
        static private int DoesThisHallmarkExist(int[] hallmarks, int hallmarksCounter, int hallmark)
        {
            for (int i = 0; i < hallmarksCounter; i++)
            {
                if (hallmarks[i] == hallmark)
                {
                    return i;
                }
            }
            return -1;
        }
        /// <summary>
        /// Funkcija ieskanti daugiausiai pasikatojancios prabos vietos masyve
        /// </summary>
        /// <param name="hallmarks">Prabu masyvas, kuriame bus ieskoma</param>
        /// <param name="hallmarksCounter">Prabu kiekis masyve</param>
        /// <param name="hallmarksCounters">Prabu pasikartojimai masyve</param>
        /// <returns>Grazina daugiausiai pasikartojancios prabos vieta masyve</returns>
        static private int MostRecurHallmark(int[] hallmarks, int hallmarksCounter, int[] hallmarksCounters)
        {
            int mostRecurHallmark = 0;
            int mostRecurHallmarkNumber = 0;
            for (int i = 0; i < hallmarksCounter; i++)
            {
                if (hallmarksCounters[i] > mostRecurHallmark)
                {
                    mostRecurHallmark = hallmarksCounters[i];
                    mostRecurHallmarkNumber = i;
                }
            }
            return mostRecurHallmarkNumber;
        }
        /// <summary>
        /// Funkcija randanti objektus Ring objektu masyve, kurie yra pigesni nei 300 ir yra pagaminti is balto aukso
        /// </summary>
        /// <param name="rings">Ring klases objektu masyvas</param>
        /// <param name="ringsCounter">Objektu kiekis Ring objektu klaseje</param>
        /// <param name="cheaperThan300Rings">Ring klases masyvas, kuris saugo Ring klases masyvo objektus, kurie yra pigesni nei 300 ir yra pagaminti is balto aukso</param>
        /// <param name="cheaperThan300RingsCounter">Pigesniu nei 300 ir pagamintu is balto aukso Ring klases objektu kiekis</param>
        static private void CheaperThan300Rings(Ring[] rings, int ringsCounter, out Ring[] cheaperThan300Rings, out int cheaperThan300RingsCounter)
        {
            cheaperThan300Rings = new Ring[MaxRingsAmount];
            cheaperThan300RingsCounter = 0;

            for (int i = 0; i < ringsCounter; i++)
            {
                if ((rings[i].Price < 300) && (rings[i].Metal == "Baltas auksas"))
                {
                    cheaperThan300Rings[cheaperThan300RingsCounter++] = rings[i];
                }
            }
        }
        /// <summary>
        /// Funkcija randanti ziedus, kuriu kaina yra tarp 300 ir 500
        /// </summary>
        /// <param name="rings">Ring klases objektu masyvas</param>
        /// <param name="ringsCounter">Ring klases objektu kiekis masyve</param>
        /// <param name="ringsBetweenInterval">Ring klases objektu masyvas, kuriame bus saugojami objektai is rings klases, kuriu kaina yra tarp 300 ir 500</param>
        /// <param name="ringsBetweenIntervalCounter">Objektu kuriu kaina tarp 300 ir 500 kiekis rings masyve</param>
        static private void ThreeRingsBetweenInterval(Ring[] rings, int ringsCounter, out Ring[] ringsBetweenInterval, out int ringsBetweenIntervalCounter)
        {
            ringsBetweenInterval = new Ring[3];
            ringsBetweenIntervalCounter = 0;

            for (int i = 0; i < ringsCounter; i++)
            {
                if ((rings[i].Price >= 300) && (rings[i].Price <= 500))
                {
                    if (ringsBetweenIntervalCounter < 3)
                    {
                        ringsBetweenInterval[ringsBetweenIntervalCounter++] = rings[i];
                    }
                    else
                    {
                        break;
                    }

                }
            }
        }
        /// <summary>
        /// Funkcija spausdinanti objektu duomenis i CSV faila, kuriu kaina yra zemesne nei 300
        /// </summary>
        /// <param name="cheaperThan300Rings">Ring klases objektu masyvas, kuriame saugojami Ring klases objektai pigesni nei 300</param>
        /// <param name="cheaperThan300RingsCounter">Objektu kiekis Ring klases masyve</param>
        /// <param name="fline">Pirma pradiniu duomenu eilute</param>
        static private void PrintCheaperThan300Rings(Ring[] cheaperThan300Rings, int cheaperThan300RingsCounter, string[] fline)
        {
            using (StreamWriter writer = new StreamWriter(@CPrint1))
            {
                writer.WriteLine("------------------------------------------------------------------------------------------");
                writer.WriteLine("|{0, -20}|{1, -20}|{2, -20}|{3, -6}|{4, -6}|{5, -4}|{6, -5}|",
                    fline[0], fline[1], fline[2], fline[3], fline[4], fline[5], fline[6]);
                writer.WriteLine("------------------------------------------------------------------------------------------");
                for (int i = 0; i < cheaperThan300RingsCounter; i++)
                {
                    writer.WriteLine("|{0, -20}|{1, -20}|{2, -20}|{3, 6}|{4,6}|{5,5}|{6,5}|", cheaperThan300Rings[i].Producer, cheaperThan300Rings[i].Name, cheaperThan300Rings[i].Metal,
                        cheaperThan300Rings[i].Weight, cheaperThan300Rings[i].Size, cheaperThan300Rings[i].Hallmark, cheaperThan300Rings[i].Price);
                    writer.WriteLine("------------------------------------------------------------------------------------------");
                }
            }
        }
        /// <summary>
        /// Funkcija spausdinanti rezultatus i CSV faila
        /// </summary>
        /// <param name="rings">Ring klases objektu masyvas</param>
        /// <param name="ringsCounter">Ring klases objektu kiekis</param>
        /// <param name="mostExpensiveRings">Ring klases objektu masyvas, kuriame saugojami objektai lygus didziausiai kainai</param>
        /// <param name="mostExpensiveCounter">Objektu kiekis mostExpensiveRings masyve</param>
        /// <param name="mostRecurHallmakNumber">Dazniausiai pasikartojanti praba Ring klases objektu masyve</param>
        /// <param name="hallmarks">Prabu masyvas</param>
        /// <param name="hallmarksCounters">Prabu pasikartojimo kiekiu masyvas</param>
        /// <param name="ringsBetweenInterval">Ring klases objektu kainuojanciu nuo 300 iki 500 masyvas</param>
        /// <param name="ringsBetweenIntervalCounter">Ring klases objektu kainuojanciu tarp 300 ir 500 elementu kiekis</param>
        static private void PrintResultsToFile(Ring[] rings, int ringsCounter, Ring[] mostExpensiveRings, int mostExpensiveCounter, int mostRecurHallmakNumber, int[] hallmarks,
            int[] hallmarksCounters, Ring[] ringsBetweenInterval, int ringsBetweenIntervalCounter, string[] fline)
        {
            using (StreamWriter writer = new StreamWriter(@CPrint2))
            {
                writer.WriteLine("--------------------------------------------------------------------------------------------");
                writer.WriteLine("|{0, -20}|{1, -20}|{2, -20}|{3, -6}|{4, -6}|{5, -4}|{6, -7}|",
                    fline[0], fline[1], fline[2], fline[3], fline[4], fline[5], fline[6]);
                writer.WriteLine("--------------------------------------------------------------------------------------------");
                for (int i = 0; i < ringsCounter; i++)
                {
                    writer.WriteLine("|{0, -20}|{1, -20}|{2, -20}|{3, 6}|{4,6}|{5,5}|{6,7}|", rings[i].Producer, rings[i].Name, rings[i].Metal, rings[i].Weight, 
                        rings[i].Size, rings[i].Hallmark, rings[i].Price);
                    writer.WriteLine("--------------------------------------------------------------------------------------------");
                }
                if (mostExpensiveCounter == 1)
                {
                    writer.WriteLine("");
                    writer.WriteLine("Brangiausias ziedas:");
                    writer.WriteLine("------------------------------------------------------------------------");
                    writer.WriteLine("|{0, -20}|{1, -20}|{2, -10}|{3, -8}|{4, -8}|",
                        fline[1], fline[2], "Skersmuo", fline[3], fline[5]);
                    writer.WriteLine("------------------------------------------------------------------------");
                    writer.WriteLine("|{0, -20}|{1, -20}|{2, 10}|{3, 8}|{4, 8}|", mostExpensiveRings[0].Name, mostExpensiveRings[0].Metal, mostExpensiveRings[0].Size,
                        mostExpensiveRings[0].Weight, mostExpensiveRings[0].Hallmark);
                    writer.WriteLine("------------------------------------------------------------------------");
                }
                else
                {
                    writer.WriteLine("");
                    writer.WriteLine("Brangiausi ziedai:");
                    writer.WriteLine("------------------------------------------------------------------------");
                    writer.WriteLine("|{0, -20}|{1, -20}|{2, -10}|{3, -8}|{4, -8}|",
                        fline[1], fline[2], "Skersmuo", fline[3], fline[5]);
                    writer.WriteLine("------------------------------------------------------------------------");
                    for (int i = 0; i < mostExpensiveCounter; i++)
                    {
                        writer.WriteLine("|{0, -20}|{1, -20}|{2, 10}|{3, 8}|{4, 8}|", mostExpensiveRings[i].Name, mostExpensiveRings[i].Metal, mostExpensiveRings[i].Size, 
                            mostExpensiveRings[i].Weight, mostExpensiveRings[i].Hallmark);
                        writer.WriteLine("----------------------------------------------------------------------");
                    }
                }
                writer.WriteLine("");
                writer.WriteLine("Parduotuveje yra daugiausiai {0} prabos ziedu, ju yra {1}", hallmarks[mostRecurHallmakNumber], hallmarksCounters[mostRecurHallmakNumber]);
                writer.WriteLine("");

                writer.WriteLine("Trys ziedai tarp intervalo (300:500):");
                writer.WriteLine("------------------------------------------------------------------------------------------");
                writer.WriteLine("|{0, -20}|{1, -20}|{2, -20}|{3, -6}|{4, -6}|{5, -4}|{6, -5}|",
                    fline[0], fline[1], fline[2], fline[3], fline[4], fline[5], fline[6]);
                writer.WriteLine("------------------------------------------------------------------------------------------");
                for (int i = 0; i < ringsBetweenIntervalCounter; i++)
                {
                    writer.WriteLine("|{0, -20}|{1, -20}|{2, -20}|{3, 6}|{4,6}|{5,5}|{6,5}|", ringsBetweenInterval[i].Producer, ringsBetweenInterval[i].Name, ringsBetweenInterval[i].Metal, ringsBetweenInterval[i].Weight,
                        ringsBetweenInterval[i].Size, ringsBetweenInterval[i].Hallmark, ringsBetweenInterval[i].Price);
                    writer.WriteLine("------------------------------------------------------------------------------------------");
                }
            }
        }
    }
}