﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labarotorinis_darbas
{
    class Ring
    {
        public string Producer { get; set; }
        public string Name { get; set; }
        public string Metal { get; set; }
        public double Weight { get; set; }
        public double Size { get; set; }
        public double Price { get; set; }
        public int Hallmark { get; set; }

        public Ring()
        {
        }
        
        public Ring(string producer, string name, string metal, double weight, double size, int hallmark, double price)
        {
            Producer = producer;
            Name = name;
            Metal = metal;
            Weight = weight;
            Size = size;
            Hallmark = hallmark;
            Price = price;
        }
    }
}
