﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._1_savarankiško_darbo_užduotis
{
    class Flat
    {
        public int Number { get; set; }
        public double Space { get; set; }
        public int RoomsCount { get; set; }
        public double Price { get; set; }
        public string TelNumber { get; set; }
        public int Floor { get; set; }

        public Flat()
        {
        }

        public Flat(int number, double space, int roomsCount, double price, string telNumber)
        {
            Number = number;
            Space = space;
            RoomsCount = roomsCount;
            Price = price;
            TelNumber = telNumber;
        }
        public override string ToString()
        {
            return String.Format("Flat Number: {0, 4} Space: {1, 5} Rooms Count: {2, 4} Floor: {3, 4} Price: {4, 12} Tel Number: {5, -8}", 
                Number, Space, RoomsCount, Floor, Price, TelNumber);
        }
    }
}
