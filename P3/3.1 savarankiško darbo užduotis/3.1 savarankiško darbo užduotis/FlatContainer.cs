﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._1_savarankiško_darbo_užduotis
{
    class FlatContainer
    {
        public Flat[] Flats { get; set; }
        public int Count { get; private set; }

        public FlatContainer(int size)
        {
            Flats = new Flat[size];
        }
        public void AddFlat (Flat flat)
        {
            Flats[Count] = flat;
            Count++;
        }
        public Flat GetFlat(int index)
        {
            return Flats[index];
        }
        public void FlatFloors()
        {
            for (int i = 0; i < Count; i++)
            {
                Flats[i].Floor = (Flats[i].Number - (Flats[i].Number % 27)) / 27;
                Flats[i].Floor = (Flats[i].Floor - (Flats[i].Floor % 3)) / 3;
                Flats[i].Floor += 1;
            }
        }
        public int Contains(Flat flat)
        {
            for(int i = 0; i < Count; i++)
            {
                for (int j = i+1; j < Count; j++)
                {
                    if (Flats[i].Number == Flats[j].Number)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        public void Remove(int floorIntervalBegin, int floorIntervalEnd, int roomsCount, double price) 
        {
            for(int i = 0; i < Count; i++)
            {
                if (Flats[i].RoomsCount != roomsCount || Flats[i].Floor < floorIntervalBegin || 
                    Flats[i].Floor > floorIntervalEnd || Flats[i].Price > price)
                {
                    for (int j = i; j < Count; j++)
                    {
                        Flats[j] = Flats[j + 1];
                    }
                    Count--;
                }
            }
        }
        public void Remove(int place)
        {
            for (int j = place; j < Count; j++)
            {
                Flats[j] = Flats[j + 1];
            }
            Count--;
        }
    }
}
