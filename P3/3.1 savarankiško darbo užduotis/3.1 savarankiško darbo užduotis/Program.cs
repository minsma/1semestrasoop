﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._1_savarankiško_darbo_užduotis
{
    class Program
    {
        const int maxStaircases = 20;
        const int maxFlatsInStaircase = 27;
        const string FileIn = "Data.csv";
        const string FileIn2 = "FlatInfo.csv";
        const string FileOut = "Results.csv";

        static void Main(string[] args)
        {
            FlatContainer flatContainer;
            ReadDataFromFile(out flatContainer);
            int roomsCount, floorIntervalBegin, floorIntervalEnd;
            double price;
            ReadFlatInfoFromFile(out roomsCount, out floorIntervalBegin, out floorIntervalEnd, out price);
            flatContainer.Remove(roomsCount, floorIntervalBegin, floorIntervalEnd, price);
            RemoveEquals(ref flatContainer);
            flatContainer.FlatFloors();
            PrintResultsToFile(flatContainer);
        }
        /// <summary>
        /// Butu dejimas i Butu konteineri
        /// </summary>
        /// <param name="flatContainer">Butu konteineris</param>
        private static void ReadDataFromFile(out FlatContainer flatsContainer)
        {
            using (StreamReader reader = new StreamReader(@FileIn))
            {
                flatsContainer = new FlatContainer(maxFlatsInStaircase * maxStaircases);

                string line = null;
                int counter = 0;
                while (null != (line = reader.ReadLine()))
                {
                    if (counter < 1)
                    {
                        line = reader.ReadLine();
                        counter++;
                    }
                    else
                    {
                        string[] values = line.Split(';');
                        int number = Convert.ToInt32(values[0]);
                        double space = Convert.ToDouble(values[1]);
                        int roomCount = Convert.ToInt32(values[2]);
                        double price = Convert.ToDouble(values[3]);
                        string telNumber = values[4];

                        Flat flat = new Flat(number, space, roomCount, price, telNumber);
                        flatsContainer.AddFlat(flat);
                    }
                }
            }
        }
        /// <summary>
        /// Nuskaito buto parametrus pagal kuriuos ieskos butu
        /// </summary>
        /// <param name="roomsCount">Kambariu kiekis</param>
        /// <param name="floorIntervalBegin">Auksto intervalo pradzia</param>
        /// <param name="floorIntervalEnd">Auksto intervalo pabaiga</param>
        /// <param name="price">Kaina</param>
        private static void ReadFlatInfoFromFile(out int roomsCount, out int floorIntervalBegin, out int floorIntervalEnd, out double price)
        {
            roomsCount = 0;
            floorIntervalBegin = 0;
            floorIntervalEnd = 0;
            price = 0;

            using (StreamReader reader = new StreamReader(@FileIn2))
            {
                string line = null;
                line = reader.ReadLine();
                string[] values = line.Split(';');
                roomsCount = Convert.ToInt32(values[0]);
                floorIntervalBegin = Convert.ToInt32(values[1]);
                floorIntervalEnd = Convert.ToInt32(values[2]);
                price = Convert.ToDouble(values[3]);
            }
        }
        private static void RemoveEquals(ref FlatContainer flatContainer)
        {
            for (int i = 0; i < flatContainer.Count; i++) 
            { 
                if(flatContainer.Contains(flatContainer.GetFlat(i)) != -1)
                {
                    flatContainer.Remove(i);
                    i--;
                }
            }
        }
        /// <summary>
        /// Atrinktu butu spausdinimas i CSV rezultatu faila
        /// </summary>
        /// <param name="flatContainer">Atrinktu butu konteineris</param>
        private static void PrintResultsToFile(FlatContainer flatContainer)
        {
            using (StreamWriter writer = new StreamWriter(@FileOut))
            {
                for (int i = 0; i < flatContainer.Count; i++)
                {
                    writer.WriteLine(flatContainer.GetFlat(i).ToString());
                }
            }
        }
    }
}
