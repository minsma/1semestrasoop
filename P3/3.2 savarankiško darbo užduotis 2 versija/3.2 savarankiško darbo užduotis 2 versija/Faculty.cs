﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis_2_versija
{
    class Faculty
    {
        public Group[] Groups { get; set; }
        public int GroupsCounter { get; set; }
        public string Name { get; set; }

        public Faculty(string name, int size)
        {
            Groups = new Group[size];
            GroupsCounter = 0;
            Name = name;
        }

        public Group TakeGroup(int place)
        {
            return Groups[place];
        }

        public void AddGroup(Group group)
        {
            Groups[GroupsCounter] = group;
            GroupsCounter++;
        }

        public int Contains(string group_name)
        {
            for(int i = 0; i < GroupsCounter; i++)
            {
                if(group_name == Groups[i].Name)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Sort()
        {
            for(int i = 0; i < GroupsCounter; i++)
            {
                for (int j = i + 1; j < GroupsCounter; j++)
                {
                    if (Groups[i].Average == Groups[j].Average && String.Compare(Groups[i].Name, Groups[j].Name) < 0)
                    {
                        Group group = Groups[i];
                        Groups[i] = Groups[j];
                        Groups[j] = group;
                    }
                    else if (Groups[i].Average < Groups[j].Average)
                    {
                        Group group = Groups[i];
                        Groups[i] = Groups[j];
                        Groups[j] = group;
                    }
                }
            }
        }

        public void Remove(int place)
        {
            for(int i = place; i < GroupsCounter; i++)
            {
                Groups[i] = Groups[i + 1];
            }
            GroupsCounter--;
        }
    }
}
