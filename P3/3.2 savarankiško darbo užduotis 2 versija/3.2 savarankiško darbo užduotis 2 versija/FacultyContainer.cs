﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis_2_versija
{
    class FacultyContainer
    {
        public Faculty[] Faculties { get; set; }
        public int FacultiesCounter { get; private set; }

        public FacultyContainer(int size)
        {
            Faculties = new Faculty[size];
            FacultiesCounter = 0;
        }

        public Faculty TakeFaculty(int place)
        {
            return Faculties[place];
        }

        public void AddFaculty(Faculty faculty)
        {
            Faculties[FacultiesCounter++] = faculty;
        }

        public void RemoveFaculty(int place)
        {
            for(int i = place; i < FacultiesCounter; i++)
            {
                Faculties[i] = Faculties[i + 1];
            }
            FacultiesCounter--;
        }

        public bool Contains(string name)
        {
            for(int i = 0; i < FacultiesCounter; i++)
            {
                if(name == Faculties[i].Name)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
