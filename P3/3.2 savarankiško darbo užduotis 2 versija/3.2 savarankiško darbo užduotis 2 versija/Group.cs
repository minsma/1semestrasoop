﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis_2_versija
{
    class Group
    {
        public Student[] Students { get; set; }
        public int StudentsCounter { get; private set; }
        public string Name { get; set; }
        public int MarksSum { get; set; }
        public int MarksCountSum { get; set; }
        public double Average { get; set; }
        
        public Group(int size, string name)
        {
            Name = name;
            Students = new Student[size];
            StudentsCounter = 0;
            MarksSum = 0;
            MarksCountSum = 0;
        }

        public Student TakeStudent(int place)
        {
            return Students[place];
        }

        public void AddStudent(Student student)
        {
            Students[StudentsCounter] = student;
            StudentsCounter++;
        }

        public void MarksSums()
        {
            for(int i = 0; i < StudentsCounter; i++)
            {
                for(int j = 0; j < Students[i].MarksCount; j++)
                {
                    MarksSum += Students[i].Marks[j];
                }
            }
        }

        public void MarksCountSums()
        {
            for(int i = 0; i < StudentsCounter; i++)
            {
                MarksCountSum += Students[i].MarksCount; 
            }
        }

        public override string ToString()
        {
            return String.Format("GrupesPav: {0, -10} Vidurkis: {1, 8}", Name, Average);
        }
    }
}
