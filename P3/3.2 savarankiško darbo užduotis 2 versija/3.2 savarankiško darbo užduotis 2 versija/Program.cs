﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis_2_versija
{
    class Program
    {
        const int MaxFaculties = 20;
        const int MaxGroups = 150;
        const int MaxStudents = 3000;
        const int MaxMarks = 25;
        const string FOut = "Results.csv";

        static void Main(string[] args)
        {
            FacultyContainer facultyContainer;

            FacultyNames(out facultyContainer);
            ReadDataFromFile(ref facultyContainer);
            SetGroupsNames(ref facultyContainer);
            GroupsMarksSums(ref facultyContainer);
            IfDublicates(ref facultyContainer);
            GroupsAverages(ref facultyContainer);
            SortGroups(ref facultyContainer);
            PrintResultsToFile(facultyContainer);
        }
        /// <summary>
        /// Funkcija nuskaitanti fakultetu pavadinimus
        /// </summary>
        /// <param name="facultyContainer">Fakultetu konteineris</param>
        static private void FacultyNames(out FacultyContainer facultyContainer)
        {
            facultyContainer = new FacultyContainer(MaxFaculties);

            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "Data*.csv");

            for (int i = 0; i < filePaths.Length; i++)
            {
                using (StreamReader reader = new StreamReader(filePaths[i]))
                {
                    string name = reader.ReadLine();
                    if (!facultyContainer.Contains(name))
                    {
                        facultyContainer.AddFaculty(new Faculty(name, MaxGroups));
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija paimanti Fakulteto vieta Fakultetu Konteinerio masyve pagal perduodama pavadinima
        /// </summary>
        /// <param name="name">Pavadinimas, kurio bus ieskoma</param>
        /// <param name="facultyContainer">Fakultetu konteineris</param>
        /// <returns></returns>
        static private int GetFacultyPlaceByName(string name, FacultyContainer facultyContainer)
        {
            int place = -1;

            for (int i = 0; i < facultyContainer.FacultiesCounter; i++)
            {
                if (name == facultyContainer.Faculties[i].Name)
                {
                    place = i;
                }
            }
            return place;
        }
        /// <summary>
        /// Funkcija skaitanti pradinius duomenis ir deda i Fakultetu Konteineri
        /// </summary>
        /// <param name="facultyContainer"></param>
        static private void ReadDataFromFile(ref FacultyContainer facultyContainer)
        {
            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "Data*.csv");

            for (int i = 0; i < filePaths.Length; i++)
            {
                using (StreamReader reader = new StreamReader(filePaths[i]))
                {
                    int counter = 0;
                    string line = reader.ReadLine();
                    string name = line;
                    int place = GetFacultyPlaceByName(name, facultyContainer);

                    Group[] groups = new Group[MaxGroups];
                    int groupsCounter = 0;

                    while (null != (line = reader.ReadLine()))
                    {
                        string[] values = line.Split(';');
                        string firstName = values[0];
                        string lastName = values[1];
                        string groupName = values[2];
                        int marksCount = Convert.ToInt32(values[3]);
                        string[] marks = values[4].Split(',');
                        int[] marks2 = new int[MaxMarks];
                        for (int j = 0; j < marksCount; j++)
                        {
                            marks2[j] = Convert.ToInt32(marks[j]);
                        }
                        Student student = new Student(firstName, lastName, groupName, marksCount, marks2);
                        if (DoesItExist(groups, groupsCounter, groupName) == -1)
                        {
                            groups[groupsCounter] = new Group(MaxStudents, groupName);
                            groups[groupsCounter].AddStudent(student);
                            groupsCounter++;
                        }
                        else
                        {
                            groups[DoesItExist(groups, groupsCounter, groupName)].AddStudent(student);
                        }
                    }
                    for (int j = 0; j < groupsCounter; j++)
                    {
                        facultyContainer.Faculties[place].AddGroup(groups[j]);
                    }
                }
            }
        }
        /// <summary>
        /// Funckija tikrinanti ar tokia grupe jau egzistuoja, jeigu egzistuoja grazinama jos tiksli vieta, grupiu klases masyve, pagal perduodama pavadinima
        /// </summary>
        /// <param name="groups">Grupiu klases masyvas</param>
        /// <param name="groupsCounter">Grupiu kiekis klases masyve</param>
        /// <param name="groups_name">Pavadinimas, pagal kuri bus ieskoma grupes vietos</param>
        /// <returns></returns>
        private static int DoesItExist(Group[] groups, int groupsCounter, string groups_name)
        {
            for (int i = 0; i < groupsCounter; i++)
            {
                if (groups[i].Name == groups_name)
                {
                    return i;
                }
            }
            return -1;
        }
        /// <summary>
        /// Funkcija suteikianti Fakultetu grupems pavadinimus
        /// </summary>
        /// <param name="facultyContainer">Fakultetu kointeineris</param>
        static private void SetGroupsNames(ref FacultyContainer facultyContainer)
        {
            for (int i = 0; i < facultyContainer.FacultiesCounter; i++)
            {
                for (int j = 0; j < facultyContainer.TakeFaculty(i).GroupsCounter; j++)
                {
                    facultyContainer.TakeFaculty(i).TakeGroup(j).Name = facultyContainer.TakeFaculty(i).TakeGroup(j).Students[0].Group;
                }
            }
        }
        /// <summary>
        /// Funkcija, kurio nustatomos pazymiu sumos ir pazymiu kiekis grupei
        /// </summary>
        /// <param name="facultyContainer">Fakultetu Konteineris</param>
        static private void GroupsMarksSums(ref FacultyContainer facultyContainer)
        {
            for (int i = 0; i < facultyContainer.FacultiesCounter; i++)
            {
                for (int j = 0; j < facultyContainer.TakeFaculty(i).GroupsCounter; j++)
                {
                    facultyContainer.TakeFaculty(i).TakeGroup(j).MarksCountSums();
                    facultyContainer.TakeFaculty(i).TakeGroup(j).MarksSums();
                }
            }
        }
        /// <summary>
        ///  Funkcija skirta besidubliuojanciu grupiu salinimui
        /// </summary>
        /// <param name="facultyContainer">Fakultetu kointeineris</param>
        static private void IfDublicates(ref FacultyContainer facultyContainer)
        {
            for(int i = 0; i < facultyContainer.FacultiesCounter; i++)
            {
                for(int j = 0; j < facultyContainer.TakeFaculty(i).GroupsCounter; j++)
                {
                    for(int z = j+1; z < facultyContainer.TakeFaculty(i).GroupsCounter; z++)
                    {
                        if(facultyContainer.TakeFaculty(i).TakeGroup(j).Name == facultyContainer.TakeFaculty(i).TakeGroup(z).Name)
                        {
                            facultyContainer.TakeFaculty(i).TakeGroup(j).MarksSum += facultyContainer.TakeFaculty(i).TakeGroup(z).MarksSum;
                            facultyContainer.TakeFaculty(i).TakeGroup(j).MarksCountSum += facultyContainer.TakeFaculty(i).TakeGroup(z).MarksCountSum;
                            facultyContainer.TakeFaculty(i).Remove(z);
                            z--;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija nustatanti grupiu vidurkius
        /// </summary>
        /// <param name="facultyContainer">Fakultetu konteineris</param>
        static private void GroupsAverages(ref FacultyContainer facultyContainer)
        {
            for(int i = 0; i < facultyContainer.FacultiesCounter; i++)
            {
                for(int j = 0; j < facultyContainer.TakeFaculty(i).GroupsCounter; j++)
                {
                    facultyContainer.TakeFaculty(i).TakeGroup(j).Average = (double) facultyContainer.TakeFaculty(i).TakeGroup(j).MarksSum / facultyContainer.TakeFaculty(i).TakeGroup(j).MarksCountSum;
                }
            }
        }
        /// <summary>
        /// Funkcija isrikiuojanti grupes, pagal vidurki ir pavadinima mazejanciai
        /// </summary>
        /// <param name="facultyContainer">Fakultetu konteineris</param>
        static private void SortGroups(ref FacultyContainer facultyContainer)
        {
            for(int i = 0; i < facultyContainer.FacultiesCounter; i++)
            {
                facultyContainer.TakeFaculty(i).Sort();
            }
        }
        /// <summary>
        /// Funkcija spausdinanti galutinius rezultatus i CSV faila
        /// </summary>
        /// <param name="facultyContainer">Fakultetu konteineris</param>
        static private void PrintResultsToFile(FacultyContainer facultyContainer)
        {
            using (StreamWriter writer = new StreamWriter(@FOut))
            {
                for(int i = 0; i < facultyContainer.FacultiesCounter; i++)
                {
                    writer.WriteLine(facultyContainer.TakeFaculty(i).Name);
                    for(int j = 0; j < facultyContainer.TakeFaculty(i).GroupsCounter; j++)
                    {
                        writer.WriteLine(facultyContainer.TakeFaculty(i).TakeGroup(j).ToString());
                    }
                }
            }
        }
    }
}
