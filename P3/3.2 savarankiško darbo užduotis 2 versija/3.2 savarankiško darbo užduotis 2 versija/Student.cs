﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis_2_versija
{
    class Student
    { 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Group { get; set; }
        public int MarksCount { get; set; }
        public int[] Marks { get; set; }

        public Student(string firstName, string lastName, string group, int marksCount, int[] marks)
        {
            FirstName = firstName;
            LastName = lastName;
            Group = group;
            MarksCount = marksCount;
            Marks = new int[marksCount];

            for(int i = 0; i < MarksCount; i++)
            {
                Marks[i] = marks[i];
            }
        }
    }
}
