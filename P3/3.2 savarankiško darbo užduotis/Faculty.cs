﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis {

    class Faculty 
    {
        public string Name { get; set; }
        public int GroupsCounter { get; private set; }
        public Group[] Groups { get; set; }

        public Faculty(int size, string name) 
        {
            GroupsCounter = 0;
            Groups = new Group[size];
            Name = name;
        }

        public void AddGroup(Group group) 
        {
            Groups[GroupsCounter++] = group;
        }

        public Group GetGroup(int index)
        {
            return Groups[index];
        }
        
        public void Sort()
        {
            for(int i = 0; i < GroupsCounter; i++)
            {
                for(int j = i+1; j < GroupsCounter; j++)
                {
                    if(Groups[i].GroupAverage() < Groups[j].GroupAverage())
                    {
                        Group temp;
                        temp = Groups[i];
                        Groups[i] = Groups[j];
                        Groups[j] = temp;
                    }
                    else if(Groups[i].GroupAverage() == Groups[j].GroupAverage() && string.Compare(Groups[i].Name, Groups[j].Name) == -1)
                    {
                        Group temp;
                        temp = Groups[i];
                        Groups[i] = Groups[j];
                        Groups[j] = temp;
                    }
                }
            }
        }

        public string ToString(Group group)
        {
            return String.Format("GrupesPav: {0, -10} Vidurkis: {1, 6}", group.Name, group.GroupAverage());
        }
    }
}
