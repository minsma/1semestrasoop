﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis
{
    class Group
    {
        public string Name { get; set; }
        public Student[] Students { get; set; }
        public int StudentsCounter { get; private set; }

        public Group(string name, int size)
        {
            StudentsCounter = 0;
            Students = new Student[size];
            Name = name;
        }

        public void AddStudent(Student student)
        {
            Students[StudentsCounter++] = student;
        }

        public Student GetStudent(int index)
        {
            return Students[index];
        }

        public double GroupAverage()
        {
            int marksSum = 0;
            int marksCount = 0;

            for (int i = 0; i < StudentsCounter; i++)
            {
                marksCount += Students[i].MarksCount;
                for (int j = 0; j < Students[i].MarksCount; j++)
                {
                    marksSum = Students[i].Marks[j];
                }
            }
            return (double) marksSum / marksCount;
        }
    }
}
