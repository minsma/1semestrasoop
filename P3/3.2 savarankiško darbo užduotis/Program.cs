﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis
{

    class Program
    {

        const int MaxFaculties = 50;
        const int MaxStudents = 40000;
        const int MaxGroups = 200;

        const string FOut = "Results.csv";

        static void Main(string[] args)
        {
            University university;

            PutDataToClasses(out university);

            //PrintResultsToFile(faculties, facultiesCounter);
        }
        /// <summary>
        /// Skaito studento duomenis is pradiniu duomenu failo, kurio pavadinimas file_name, i Student klases masyva, 
        /// nuskaito pradiniu duomenu faile esanti fakulteto pavadinima ir skaiciuoja studentu kieki
        /// </summary>
        /// <param name="students">Studentu klases masyvas</param>
        /// <param name="studentsCounter">Studentu klases masyve esanciu studentu kiekis</param>
        /// <param name="name">Pradiniu duomenu faile esancio fakulteto pavadinimas</param>
        /// <param name="file_name">Pradiniu duomenu failo pavadinimas</param>
        static void ReadDataFromFile(out Student[] students, out int studentsCounter, out string name, string file_name)
        {
            studentsCounter = 0;
            students = new Student[MaxStudents];

            using (StreamReader reader = new StreamReader(@file_name))
            {
                string line = null;
                name = reader.ReadLine();

                while (null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(';');
                    string lastName = values[0];
                    string firstName = values[1];
                    string group = values[2];
                    int marksCount = Convert.ToInt32(values[3]);
                    int[] marks = new int[values[4].Length];
                    string[] marks2 = values[4].Split(',');
                    for (int i = 0; i < marksCount; i++)
                    {
                        marks[i] = Convert.ToInt32(marks2[i]);
                    }
                    students[studentsCounter++] = new Student(lastName, firstName, group, marksCount, marks);
                }
            }
        }
        /// <summary>
        /// Funkcija pradinius duomenis paskirstanti i klases
        /// </summary>
        /// <param name="faculties">Fakultetu klases masyvas</param>
        /// <param name="facultiesCounter">Fakultetu klases masyve esanciu fakultetu kiekis</param>
        static void PutDataToClasses(out University university)
        {
            Faculty[] faculties = new Faculty[MaxFaculties];
            int facultiesCounter = 0;

            Student[] students;
            int studentsCounter;

            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "Data*.csv"); // Gaunami failu pavadinimai, kurie prasideda Data zodziu ir baigiasi csv galune

            university = new University(filePaths.Length);

            int dataFilesCount = filePaths.Length; // Gaunamas duomenu failo kiekis

            string name; // pries idedant i Fakultetu masyva saugojami fakultetu pavadinimai

            for (int i = 0; i < dataFilesCount; i++)
            {
                ReadDataFromFile(out students, out studentsCounter, out name, filePaths[i]);

                if (DoesThisFacultyExists(faculties, facultiesCounter, name) == -1) 
                    // Tikrinama ar toks fakultetas jau egzistavo,
                    //jeigu neegzistavo sukuriamas naujas fakultetas ir i jo vieta pridedamos grupes
                {
                    Group[] groups = new Group[MaxGroups];
                    int groupsCounter = 0;

                    PutStudentsToGroups(students, studentsCounter, ref groups, ref groupsCounter);

                    faculties[facultiesCounter] = new Faculty(MaxFaculties, name);

                    PutGroupsToFaculty(groups, groupsCounter, ref faculties[facultiesCounter]);
                    facultiesCounter++;
                }
                else // jeigu egzistavo i jo vieta Fakultetu klases masyve pridedamos grupes
                {
                    Group[] groups = new Group[MaxGroups];
                    int groupsCounter = 0;

                    PutStudentsToGroups(students, studentsCounter, ref groups, ref groupsCounter);

                    PutGroupsToFaculty(groups, groupsCounter, ref faculties[DoesThisFacultyExists(faculties, facultiesCounter, name)]);
                }
            }
        }
        /// <summary>
        /// Funkcija tikrinanti ar perduodamas fakulteto pavadinimas, jau egzistuoja fakultetu masyve
        /// </summary>
        /// <param name="faculties">Fakultetu klases mases</param>
        /// <param name="facultiesCounter">Fakultetu klases masyve esanciu fakultetu kiekis</param>
        /// <param name="name">Tikrinamojo fakulteto pavadinimas, pagal kuri ieskoma ar yra vienodas fakultetas masyve</param>
        /// <returns></returns>
        static int DoesThisFacultyExists(Faculty[] faculties, int facultiesCounter, string name)
        {
            for (int i = 0; i < facultiesCounter; i++)
            {
                if (faculties[i].Name == name)
                {
                    return i;
                }
            }
            return -1;
        }
        /// <summary>
        /// Funckija dedanti studentus is Students klases masyvo i grupes Groups klases masyve, pagal grupes pavadinima
        /// </summary>
        /// <param name="students">Studentu klases masyvas</param>
        /// <param name="studentsCounter">Studentu klases masyve esanciu studentu kiekis</param>
        /// <param name="groups">Grupiu klases masyvas</param>
        /// <param name="groupsCounter">Grupiu klases masyve esanciu grupiu kiekis</param>
        static void PutStudentsToGroups(Student[] students, int studentsCounter, ref Group[] groups, ref int groupsCounter)
        {
            for (int i = 0; i < studentsCounter; i++)
            {
                if (DoesThisGroupExist(groups, groupsCounter, students[i].Group) == -1)
                {
                    groups[groupsCounter] = new Group(students[i].Group, MaxGroups);
                    groups[groupsCounter].AddStudent(students[i]);
                    groupsCounter++;
                }
                else
                {
                    groups[DoesThisGroupExist(groups, groupsCounter, students[i].Group)].AddStudent(students[i]);
                }
            }
        }
        /// <summary>
        /// Funkcija tikrinanti ar jau tokia grupe egzistuoja grupiu masyve
        /// </summary>
        /// <param name="groups">Grupiu klases masyvas</param>
        /// <param name="groupsCounter">Grupiu klases masyve esanciu grupiu kiekis</param>
        /// <param name="name">Grupes pavadinimas, pagal kuri bus ieskoma vienodu grupiu</param>
        /// <returns></returns>
        static int DoesThisGroupExist(Group[] groups, int groupsCounter, string name)
        {
            for (int i = 0; i < groupsCounter; i++)
            {
                if (groups[i].Name == name)
                {
                    return i;
                }
            }
            return -1;
        }
        /// <summary>
        /// Funkcija dedanti grupes i fakultetu klases masyva
        /// </summary>
        /// <param name="groups">Grupiu klases masyvas</param>
        /// <param name="groupsCounter">Grupiu klases masyve esanciu grupiu kiekis</param>
        /// <param name="faculty">Fakultetu klases masyvas</param>
        static void PutGroupsToFaculty(Group[] groups, int groupsCounter, ref Faculty faculty)
        {
            for (int i = 0; i < groupsCounter; i++)
            {
                faculty.AddGroup(groups[i]);
            }
        }
        /// <summary>
        /// Funkcija spausdinanti galutinius rezultatus i Results.CSV faila
        /// </summary>
        /// <param name="faculties">Fakultetu klases masyvas</param>
        /// <param name="facultiesCounter">Fakultetu klases masyve esanciu fakultetu kiekis</param>
        static void PrintResultsToFile(Faculty[] faculties, int facultiesCounter)
        {
            using (StreamWriter writer = new StreamWriter(@FOut))
            {
                for (int i = 0; i < facultiesCounter; i++)
                {
                    writer.WriteLine(faculties[i].Name);
                    writer.WriteLine(faculties[i].GroupsCounter);
                    for (int j = 0; j < faculties[i].GroupsCounter; j++)
                    {
                        faculties[i].Sort(); // Ivykdoma duomenu rikiavimo funkcija pagal vidurki mazejanciai ir pagal grupes pavadinima abeceliskai mazejanciai
                        writer.WriteLine(faculties[i].ToString(faculties[i].Groups[j]));
                    }
                }
            }
        }
    }
}








