﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis {
    
    class Student 
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Group { get; set; }
        public int MarksCount { get; set; }
        public int[] Marks { get; set; }

        public Student() 
        {
        }

        public Student(string lastName, string firstName, string group, int marksCount, int[] marks)
        {
            LastName = lastName;
            FirstName = firstName;
            Group = group;
            MarksCount = marksCount;
            Marks = new int[MarksCount];
            for (int i = 0; i < MarksCount; i++)
            { 
                Marks[i] = marks[i];
            }
        }
    }
}
