﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._2_savarankiško_darbo_užduotis
{
    class University
    {
        Faculty[] Faculties;
        int FacultiesCounter;

        public University(int size) 
        {
            Faculties = new Faculty[size];
            FacultiesCounter++;
        }

        public void AddFaculty(Faculty faculty) 
        {
            Faculties[FacultiesCounter++] = faculty;
        }

        public int Contains(string name) 
        {
            for (int i = 0; i < FacultiesCounter; i++) 
            {
                if (name == Faculties[i].Name)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
