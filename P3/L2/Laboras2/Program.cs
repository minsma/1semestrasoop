﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboras2
{
    class Program
    {
        const int MaxRingsAmount = 50; // maksimalus ziedu kiekis
        const int MaxShopsAmount = 20; // maksimalus parduotuviu kiekis
        const string PPrimaryData = "PrimaryData.csv"; // failas, kuriame atspausdinami pradiniai duomenys
        const string PUnique = "Unikalūs.csv"; // atrinktu unikaliu ziedu rezultatu failas
        const string PBA300 = "BA300.csv"; // atrinktu balto aukso pigesniu nei 300 ziedu rezultatu failas

        static void Main(string[] args)
        {
            ShopContainer shopContainer;
            Ring mostExpensive;
            Ring[] duplicates;
            Shop[] shops, whiteGoldRings, uniqueRingsShops;
            int shopsCounter, whiteGoldRingsCounter, duplicatesCounter, uniqueRingsShopsCounter;

            ReadDataFromFile(out shopContainer);
            PrintDataToFile(shopContainer);
            mostExpensive = MostExpensiveRing(shopContainer);
            MostExpensiveRingShops(shopContainer, mostExpensive, out shops, out shopsCounter);
            RemoveShopsDublicates(ref shopContainer);
            RemoveRingsDublicatesInShops(ref shopContainer);
            WhiteGoldRings(shopContainer, out whiteGoldRings, out whiteGoldRingsCounter);
            DuplicatesRings(shopContainer, out duplicates, out duplicatesCounter);
            UniqueRings(shopContainer, out uniqueRingsShops, out uniqueRingsShopsCounter, duplicates, duplicatesCounter);
            PrintUnique(uniqueRingsShops, uniqueRingsShopsCounter);
            PrintPBA300(whiteGoldRings, whiteGoldRingsCounter);
            PrintToConsole(mostExpensive, shops, shopsCounter);
        }
        /// <summary>
        /// Funckija skaitanti pradinius duomenis ir dedanti juos i parduotuviu konteineri
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        static void ReadDataFromFile(out ShopContainer shopContainer)
        {
            shopContainer = new ShopContainer(MaxShopsAmount);

            Shop[] shops = new Shop[MaxShopsAmount];

            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "Data*.csv");

            for (int i = 0; i < filePaths.Length; i++)
            {
                using (StreamReader reader = new StreamReader(filePaths[i]))
                {
                    string Name = reader.ReadLine();
                    string Address = reader.ReadLine();
                    string Phone = reader.ReadLine();

                    shops[i] = new Shop(Name, Address, Phone, MaxRingsAmount);

                    string line = null;
                    int counter = 0;
                    while (null != (line = reader.ReadLine()))
                    {
                        if (counter > 0)
                        {
                            string[] values = line.Split(';');
                            string producer = values[0];
                            string name = values[1];
                            string metal = values[2];
                            double weight = Convert.ToDouble(values[3]);
                            double size = Convert.ToDouble(values[4]);
                            int hallmark = Convert.ToInt32(values[5]);
                            double price = Convert.ToDouble(values[6]);

                            Ring ring = new Ring(producer, name, metal, weight, size, hallmark, price);

                            //shops[i].AddRing(ring);
                            shops[i] = shops[i] + ring;
                        }
                        else
                        {
                            counter++;
                        }
                    }
                    shopContainer.AddShop(shops[i]);
                }
            }
        }
        /// <summary>
        /// Funkcija spausdinanti nuskaitytus pradinius duomenis i CSV faila
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        static void PrintDataToFile(ShopContainer shopContainer)
        {
            using (StreamWriter writer = new StreamWriter(@PPrimaryData))
            {
                for (int i = 0; i < shopContainer.ShopsCounter; i++)
                {
                    writer.WriteLine(shopContainer.TakeShop(i));
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    writer.WriteLine("{0,-25}|{1, -15}|{2, -20}|{3, -10}|{4, -10}|{5, -10}|{6, -10}|",
                        "Gamintojas", "Pavadinimas", "Metalas", "Svoris", "Dydis", "Praba", "Kaina");
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    for (int j = 0; j < shopContainer.TakeShop(i).RingsCounter; j++)
                    {
                        writer.WriteLine(shopContainer.TakeShop(i).TakeRing(j));
                        writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija surandanti pati brangiausia zieda
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        /// <returns>Grazinamas pacio brangiausio ziedo objektas</returns>
        static Ring MostExpensiveRing(ShopContainer shopContainer)
        {
            Ring biggest = new Ring("", "", "", 0, 0, 0, 0);
            for (int i = 0; i < shopContainer.ShopsCounter; i++)
            {
                for (int j = 0; j < shopContainer.TakeShop(i).RingsCounter; j++)
                {
                    if (shopContainer.TakeShop(i).TakeRing(j) > biggest)
                    {
                        biggest = shopContainer.TakeShop(i).TakeRing(j);
                    }
                }
            }
            return biggest;
        }
        /// <summary>
        /// Pasalinamos pasikartojancios parduotuves, o ziedai perkeliami is pasalintos i jos dublikata
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        static void RemoveShopsDublicates(ref ShopContainer shopContainer)
        {
            for (int i = 0; i < shopContainer.ShopsCounter; i++)
            {
                for (int j = i + 1; j < shopContainer.ShopsCounter; j++)
                {
                    if (shopContainer.TakeShop(i).Equals(shopContainer.TakeShop(j)))
                    {
                        for (int z = 0; z < shopContainer.TakeShop(j).RingsCounter; z++)
                        {
                            shopContainer.TakeShop(i).AddRing(shopContainer.TakeShop(j).TakeRing(z));
                        }
                        shopContainer.Remove(j);
                        j--;
                    }
                }
            }
        }
        /// <summary>
        /// Funckija pasalinanti pasikartojancius ziedus is parduotuviu
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        static void RemoveRingsDublicatesInShops(ref ShopContainer shopContainer)
        {
            for (int i = 0; i < shopContainer.ShopsCounter; i++)
            {
                for (int j = 0; j < shopContainer.TakeShop(i).RingsCounter; j++)
                {
                    for (int z = j + 1; z < shopContainer.TakeShop(i).RingsCounter; z++)
                    {
                        if (shopContainer.TakeShop(i).TakeRing(j).Equals(shopContainer.TakeShop(i).TakeRing(z)))
                        {
                            shopContainer.TakeShop(i).Remove(z);
                            z--;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Suranda parduotuves, kuriose galima nusipirkti brangiausia zieda
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        /// <param name="mostExpensive">Brangiausias ziedas</param>
        /// <param name="shops">Parduotuviu, kuriose galima nusipirkti brangiausia zieda masyvas</param>
        /// <param name="shopCounter">Parduotuviu, kuriose galima nusipirkt brangiausia zieda kiekis</param>
        static void MostExpensiveRingShops(ShopContainer shopContainer, Ring mostExpensive, out Shop[] shops, out int shopCounter)
        {
            shops = new Shop[MaxShopsAmount];
            shopCounter = 0;

            for (int i = 0; i < shopContainer.ShopsCounter; i++)
            {
                for (int j = 0; j < shopContainer.TakeShop(i).RingsCounter; j++)
                {
                    if (mostExpensive.Equals(shopContainer.TakeShop(i).TakeRing(j)))
                    {
                        shops[shopCounter++] = shopContainer.TakeShop(i);
                    }
                }
            }
        }
        /// <summary>
        /// Parduotuves, kuriose galima nusipirkt balto aukso ziedus pigesnius nei 300 ir ziedai
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        /// <param name="whiteGoldRings">Parduotuviu, kuriose galima nusipirkt balto aukso ziedu pigesniu nei 300 masyvas</param>
        /// <param name="whiteGoldRingsCounter">Parduotuviu, kuriose galima nusipirkt balto aukso ziedu pingesniu nei 300 masyvas</param>
        static void ReadWhiteRingPriceIntervals(out double intervalBegin, out double intervalEnd)
        {
            intervalBegin = Convert.ToDouble(Console.ReadLine());
            intervalEnd = Convert.ToDouble(Console.ReadLine());
        }
        static void WhiteGoldRings(ShopContainer shopContainer, out Shop[] whiteGoldRings, out int whiteGoldRingsCounter)
        {
            whiteGoldRings = new Shop[MaxShopsAmount];
            whiteGoldRingsCounter = 0;

            double intervalBegin, intervalEnd;

            ReadWhiteRingPriceIntervals(out intervalBegin, out intervalEnd);

            for (int i = 0; i < shopContainer.ShopsCounter; i++)
            {
                int was = 0;

                whiteGoldRings[whiteGoldRingsCounter] = new Shop(shopContainer.TakeShop(i).Name, shopContainer.TakeShop(i).Address,
                            shopContainer.TakeShop(i).Phone, MaxRingsAmount);

                for (int j = 0; j < shopContainer.TakeShop(i).RingsCounter; j++)
                {
                    if (shopContainer.TakeShop(i).TakeRing(j).Metal == "Baltas auksas" && shopContainer.TakeShop(i).TakeRing(j).Price >= intervalBegin &&
                        shopContainer.TakeShop(i).TakeRing(j).Price <= intervalEnd)
                    {
                        whiteGoldRings[whiteGoldRingsCounter].AddRing(shopContainer.TakeShop(i).TakeRing(j));
                        was = 1;
                    }
                }

                if (was > 0)
                {
                    whiteGoldRingsCounter++;
                }
            }
        }
        /// <summary>
        /// Funkcija ieskanti besikartojanciu ziedu
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        /// <param name="duplicates">Pasikartojanciu ziedu masyvas</param>
        /// <param name="duplicatesCounter">Pasikartojanciu ziedu kiekis</param>
        static void DuplicatesRings(ShopContainer shopContainer, out Ring[] duplicates, out int duplicatesCounter)
        {
            duplicates = new Ring[MaxRingsAmount];
            duplicatesCounter = 0;

            for (int i = 0; i < shopContainer.ShopsCounter; i++)
            {
                for (int z = 0; z < shopContainer.TakeShop(i).RingsCounter; z++)
                {
                    for (int j = i + 1; j < shopContainer.ShopsCounter; j++)
                    {
                        if (shopContainer.TakeShop(j).Contains(shopContainer.TakeShop(i).TakeRing(z)))
                        {
                            duplicates[duplicatesCounter] = shopContainer.TakeShop(i).TakeRing(z);
                            duplicatesCounter++;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija tikrinanti ar tas ziedas yra besikartojantis
        /// </summary>
        /// <param name="duplicates">Pasikartojanciu ziedu masyvas</param>
        /// <param name="duplicatesCounter">Pasikartojanciu ziedu kiekis</param>
        /// <param name="ring_name">Ziedo pavadinimas</param>
        /// <returns>Grazina true, jeigu ziedai pasikartoja ir false priesingu atveju</returns>
        static bool DoesThatRingDuplicate(Ring[] duplicates, int duplicatesCounter, string ring_name)
        {
            for (int i = 0; i < duplicatesCounter; i++)
            {
                if (duplicates[i] == ring_name)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Funckija formuojanti unikaliu ziedu parduotuviu masyva
        /// </summary>
        /// <param name="shopContainer">Parduotuviu konteineris</param>
        /// <param name="uniqueRingsShops">Unikaliu ziedu parduotuviu masyvas</param>
        /// <param name="uniqueRingsShopsCounter">Unikaliu ziedu parduotuviu kiekis</param>
        /// <param name="duplicates">Besikartojanciu ziedu masyvas</param>
        /// <param name="duplicatesCounter">Besikartojanciu ziedu kiekis</param>
        static void UniqueRings(ShopContainer shopContainer, out Shop[] uniqueRingsShops, out int uniqueRingsShopsCounter, Ring[] duplicates, int duplicatesCounter)
        {
            uniqueRingsShops = new Shop[MaxShopsAmount];
            uniqueRingsShopsCounter = 0;

            for (int i = 0; i < shopContainer.ShopsCounter; i++)
            {
                int was = 0; // jeigu buvo bent vienas nepasikartojes ziedas, tuomet sis kintamasis tampa 1
                uniqueRingsShops[uniqueRingsShopsCounter] = new Shop(shopContainer.TakeShop(i).Name, shopContainer.TakeShop(i).Address, shopContainer.TakeShop(i).Phone, MaxRingsAmount);
                for (int z = 0; z < shopContainer.TakeShop(i).RingsCounter; z++)
                {
                    int times = 0; // kintamasis, kuriame skaiciuojama kiek kartu pasikartojo ziedas tarp visu parduotuviu
                    for (int j = i + 1; j < shopContainer.ShopsCounter; j++)
                    {
                        if (shopContainer.TakeShop(j).Contains(shopContainer.TakeShop(i).TakeRing(z)))
                        {
                            times++;
                        }
                    }
                    if (times == 0 && !DoesThatRingDuplicate(duplicates, duplicatesCounter, shopContainer.TakeShop(i).TakeRing(z).Name))
                    {
                        uniqueRingsShops[uniqueRingsShopsCounter].AddRing(shopContainer.TakeShop(i).TakeRing(z));
                        was = 1;
                    }
                }
                if (was == 1)
                {
                    uniqueRingsShopsCounter++;

                }
            }
        }
        /// <summary>
        /// Funkcija spausdinanti unikalius ziedus i CSV faila
        /// </summary>
        /// <param name="uniqueRingsShops">Unikaliu ziedu parduotuviu masyvas</param>
        /// <param name="uniqueRingsShopsCounter">Unikaliu ziedu parduotuviu kiekis</param>
        static void PrintUnique(Shop[] uniqueRingsShops, int uniqueRingsShopsCounter)
        {
            using (StreamWriter writer = new StreamWriter(@PUnique))
            {
                for (int i = 0; i < uniqueRingsShopsCounter; i++)
                {
                    writer.WriteLine(uniqueRingsShops[i]);
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    writer.WriteLine("{0,-25}|{1, -15}|{2, -20}|{3, -10}|{4, -10}|{5, -10}|{6, -10}|",
                        "Gamintojas", "Pavadinimas", "Metalas", "Svoris", "Dydis", "Praba", "Kaina");
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    for (int j = 0; j < uniqueRingsShops[i].RingsCounter; j++)
                    {
                        writer.WriteLine(uniqueRingsShops[i].TakeRing(j));
                        writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija spausdinanti informacija apie balto aukso ziedus pigesnius nei 300 i CSV faila
        /// </summary>
        /// <param name="whiteGoldRings">Balto aukso ziedu pigesniu nei 300 parduotuviu masyvas</param>
        /// <param name="whiteGoldRingsCounter">Balto aukso ziedu pigesniu nei 300 parduotuviu kiekis</param>
        static void PrintPBA300(Shop[] whiteGoldRings, int whiteGoldRingsCounter)
        {
            using (StreamWriter writer = new StreamWriter(@PBA300))
            {
                for (int i = 0; i < whiteGoldRingsCounter; i++)
                {
                    writer.WriteLine(whiteGoldRings[i]);
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    writer.WriteLine("{0,-25}|{1, -15}|{2, -20}|{3, -10}|{4, -10}|{5, -10}|{6, -10}|",
                        "Gamintojas", "Pavadinimas", "Metalas", "Svoris", "Dydis", "Praba", "Kaina");
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    for (int j = 0; j < whiteGoldRings[i].RingsCounter; j++)
                    {
                        writer.WriteLine(whiteGoldRings[i].TakeRing(j));
                        writer.WriteLine("-----------------------------------------------------------------------------------------------------------");
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija spausdinanti informacija apie brangiausia zieda ir parduotuves, kuriose galima rasti ta zieda i konsole
        /// </summary>
        /// <param name="mostExpensive">Brangiausias ziedas</param>
        /// <param name="shops">Parduotuviu masyvas, kuriose galima rasti brangiausia zieda</param>
        /// <param name="shopsCounter">Parduotuviu kiekis, kuriose galima rasti brangiausia zieda</param>
        static void PrintToConsole(Ring mostExpensive, Shop[] shops, int shopsCounter)
        {
            Console.WriteLine("Brangiausias ziedas:");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------");
            Console.WriteLine("{0,-25}|{1, -15}|{2, -20}|{3, -10}|{4, -10}|{5, -10}|{6, -10}|",
                "Gamintojas", "Pavadinimas", "Metalas", "Svoris", "Dydis", "Praba", "Kaina");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------");
            Console.WriteLine(mostExpensive);
            Console.WriteLine("-----------------------------------------------------------------------------------------------------------");
            Console.WriteLine("\r\nJi galima isigyti:");
            for(int i = 0; i < shopsCounter; i++)
            {
                Console.WriteLine("\r\n" + shops[i]);
            }
        }
    }
}