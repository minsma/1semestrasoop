﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboras2
{
    class Ring
    {
        public string Producer { get; private set; }
        public string Name { get; private set; }
        public string Metal { get; private set; }
        public double Weight { get; private set; }
        public double Size { get; private set; }
        public double Price { get; private set; }
        public int Hallmark { get; private set; }
        public Ring(string producer, string name, string metal, double weight, double size, int hallmark, double price)
        {
            Producer = producer;
            Name = name;
            Metal = metal;
            Weight = weight;
            Size = size;
            Hallmark = hallmark;
            Price = price;
        }
        public override string ToString()
        {
            return String.Format("{0,-25}|{1, -15}|{2, -20}|{3, 10}|{4, 10}|{5, 10}|{6, 10}|", 
                Producer, Name, Metal, Weight, Size, Hallmark, Price);
        }
        public static bool operator >(Ring first, Ring second)
        {
            return first.Price > second.Price;
        }
        public static bool operator <(Ring first, Ring second)
        {
            return first.Price < second.Price;
        }
        public static bool operator ==(Ring first, Ring second)
        {
            return first.Price == second.Price;
        }
        public static bool operator ==(Ring first, string second)
        {
            return first.Name == second;
        }
        public static bool operator !=(Ring first, string second)
        {
            return first.Name != second;
        }
        public static bool operator !=(Ring first, Ring second)
        {
            return first.Price != second.Price;
        }
        public bool Equals(Ring ring)
        {
            if(Object.ReferenceEquals(ring, null))
            {
                return false;
            }
            else if (this.GetType() != ring.GetType())
            {
                return false;
            }
            return ((Producer == ring.Producer) && (Name == ring.Name) && (Metal == ring.Metal) &&
                (Weight == ring.Weight) && (Size == ring.Size) && (Hallmark == ring.Hallmark));
        }
        public override int GetHashCode()
        {
            return Producer.GetHashCode() ^ Name.GetHashCode() ^ Metal.GetHashCode() ^
                Weight.GetHashCode() ^ Size.GetHashCode() ^ Price.GetHashCode() ^ Hallmark.GetHashCode();
        }
    }
}
