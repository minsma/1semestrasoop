﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboras2
{
    class Shop
    {
        public Ring[] Rings { get; set; }
        public int RingsCounter { get; private set; }
        public string Name { get; private set; }
        public string Address { get; private set; }
        public string Phone { get; private set; }

        public Shop(string name, string address, string phone, int size)
        {
            Name = name;
            Address = address;
            Phone = phone;
            Rings = new Ring[size];
            RingsCounter = 0;
        }

        public void AddRing(Ring ring)
        {
            Rings[RingsCounter] = ring;
            RingsCounter++;
        }

        public Ring TakeRing(int place)
        {
            return Rings[place];
        }

        public override string ToString()
        {
            return String.Format("{0}\r\n{1}\r\n{2}", Name, Address, Phone);
        }

        public void Remove(int place)
        {
            for (int i = place; i < RingsCounter; i++)
            {
                Rings[i] = Rings[i + 1];
            }
            RingsCounter--;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public static Shop operator + (Shop shop, Ring ring)
        {
            shop.AddRing(ring);
            return shop;
        }

        public bool Equals(Shop shop)
        {
            if (Object.ReferenceEquals(shop, null))
            {
                return false;
            }

            if (GetType() != shop.GetType())
            {
                return false;
            }

            return (Name == shop.Name) && (Address == shop.Address) && (Phone == shop.Phone);
        }

        public bool Contains(Ring ring)
        {
            for(int i = 0; i < RingsCounter; i++)
            {
                if(Rings[i].Name == ring.Name)
                {
                    return true;
                }
            }
            return false;
        }


    }
}
