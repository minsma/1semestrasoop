﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboras2
{
    class ShopContainer
    {
        private Shop[] Shops;
        public int ShopsCounter { get; private set; }

        public ShopContainer(int size)
        {
            Shops = new Shop[size];
            ShopsCounter = 0;
        }

        public void AddShop(Shop shop)
        {
            Shops[ShopsCounter] = shop;
            ShopsCounter++;
        }

        public Shop TakeShop(int place)
        {
            return Shops[place];
        }

        public void Remove(int place)
        {
            for (int i = place; i < ShopsCounter; i++)
            {
                Shops[i] = Shops[i + 1];
            }
            ShopsCounter--;
        }
    }
}
