﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4laboras
{
    class Grandinele : JuvelyrinisGaminys
    {
        public double Ilgis { get; set; }

        public Grandinele(string gamintojas, string pavadinimas, string metalas, double svoris,
            int praba, double kaina, double ilgis)
            : base(gamintojas, pavadinimas, metalas, svoris, praba, kaina)
        {
            Ilgis = ilgis;
        }

        public override string ToString()
        {
            return String.Format("|{0,-20}|{1,-20}|{2,-15}|{3, 6}|{4,6}|{5,6}|{6,6}|", Gamintojas, Pavadinimas, Metalas, Svoris, Praba, Kaina, Ilgis);
        }
    }
}
