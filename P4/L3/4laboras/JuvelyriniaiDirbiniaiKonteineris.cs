﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4laboras
{
    class JuvelyriniaiDirbiniaiKonteineris
    {
        private JuvelyrinisGaminys[] JuvelyriniaiGaminiai;
        public int Kiekis { get; set; }

        public JuvelyriniaiDirbiniaiKonteineris(int size)
        {
            JuvelyriniaiGaminiai = new JuvelyrinisGaminys[size];
            Kiekis = 0;
        }

        public void PridetiJuvelyrinisGaminys(JuvelyrinisGaminys juvelyrinisGaminys) 
        {
            JuvelyriniaiGaminiai[Kiekis] = juvelyrinisGaminys;
            Kiekis++;
        }

        public JuvelyrinisGaminys PaimtiJuvelyriniGamini(int vieta)
        {
            return JuvelyriniaiGaminiai[vieta];
        }

        public void SalintiJuvelyrinisGaminys(int vieta) 
        {
            for (int i = vieta; i < Kiekis; i++) 
            {
                JuvelyriniaiGaminiai[i] = JuvelyriniaiGaminiai[i + 1];
            }
            Kiekis--;
        }

        public bool Contains(JuvelyrinisGaminys juvelyrinisGaminys)
        {
            return JuvelyriniaiGaminiai.Contains(juvelyrinisGaminys);
        }

        public void Rikiuoti()
        {
            for(int i = 0; i < Kiekis; i++)
            {
                for(int j = i + 1; j < Kiekis; j++)
                {
                    if (JuvelyriniaiGaminiai[i].Pavadinimas.CompareTo(JuvelyriniaiGaminiai[j].Pavadinimas) == 1)
                    {
                        JuvelyrinisGaminys juvelyrinisGaminys = JuvelyriniaiGaminiai[i];
                        JuvelyriniaiGaminiai[i] = JuvelyriniaiGaminiai[j];
                        JuvelyriniaiGaminiai[j] = juvelyrinisGaminys;
                    }
                    else if (JuvelyriniaiGaminiai[i].Pavadinimas.CompareTo(JuvelyriniaiGaminiai[j].Pavadinimas) == 0 && JuvelyriniaiGaminiai[i].Gamintojas.CompareTo(JuvelyriniaiGaminiai[j].Gamintojas) == 1)
                    {
                        JuvelyrinisGaminys juvelyrinisGaminys = JuvelyriniaiGaminiai[i];
                        JuvelyriniaiGaminiai[i] = JuvelyriniaiGaminiai[j];
                        JuvelyriniaiGaminiai[j] = juvelyrinisGaminys;
                    }
                    else if(JuvelyriniaiGaminiai[i].Pavadinimas.CompareTo(JuvelyriniaiGaminiai[j].Pavadinimas) == 0 && JuvelyriniaiGaminiai[i].Gamintojas.CompareTo(JuvelyriniaiGaminiai[j].Gamintojas) == 0 && JuvelyriniaiGaminiai[i] < JuvelyriniaiGaminiai[j])
                    {
                        JuvelyrinisGaminys juvelyrinisGaminys = JuvelyriniaiGaminiai[i];
                        JuvelyriniaiGaminiai[i] = JuvelyriniaiGaminiai[j];
                        JuvelyriniaiGaminiai[j] = juvelyrinisGaminys;
                    }
                }
            }
        }
    }
}
