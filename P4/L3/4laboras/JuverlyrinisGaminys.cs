﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4laboras
{
    abstract class JuvelyrinisGaminys
    {
        public string Gamintojas { get; set; }
        public string Pavadinimas { get; set; }
        public string Metalas { get; set; }
        public double Svoris { get; set; }
        public int Praba { get; set; }
        public double Kaina { get; set; }

        public JuvelyrinisGaminys(string gamintojas, string pavadinimas, string metalas, double svoris,
            int praba, double kaina)
        {
            Gamintojas = gamintojas;
            Pavadinimas = pavadinimas;
            Metalas = metalas;
            Svoris = svoris;
            Praba = praba;
            Kaina = kaina;
        }

        public static bool operator >(JuvelyrinisGaminys gaminys1, JuvelyrinisGaminys gaminys2)
        {
            return gaminys1.Kaina > gaminys2.Kaina;
        }

        public static bool operator ==(JuvelyrinisGaminys gaminys1, JuvelyrinisGaminys gaminys2)
        {
            return gaminys1.Pavadinimas == gaminys2.Pavadinimas;
        }

        public static bool operator !=(JuvelyrinisGaminys gaminys1, JuvelyrinisGaminys gaminys2)
        {
            return gaminys1.Pavadinimas != gaminys2.Pavadinimas;
        }

        public static bool operator <(JuvelyrinisGaminys gaminys1, JuvelyrinisGaminys gaminys2)
        {
            return gaminys1.Kaina < gaminys2.Kaina;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as JuvelyrinisGaminys);
        }

        public bool Equals(JuvelyrinisGaminys juvelyrinisGaminys)
        {
            if(object.ReferenceEquals(juvelyrinisGaminys, null))
            {
                return false;
            }
            
            if(this.GetType() != juvelyrinisGaminys.GetType())
            {
                return false;
            }

            return Pavadinimas == juvelyrinisGaminys.Pavadinimas;
        }

        public override int GetHashCode()
        {
            return Pavadinimas.GetHashCode();
        }
    }
}
