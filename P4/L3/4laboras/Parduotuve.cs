﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4laboras
{
    class Parduotuve
    {
        public string Pavadinimas { get; set; }
        public string Adresas { get; set; }
        public string Telefonas { get; set; }
        public JuvelyriniaiDirbiniaiKonteineris JuvelyriniaiDirbiniai;

        public Parduotuve(string pavadinimas, string adresas, string telefonas) 
        {
            Pavadinimas = pavadinimas;
            Adresas = adresas;
            Telefonas = telefonas;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as Parduotuve);
        }

        public bool Equals(Parduotuve parduotuve)
        {
            if (object.ReferenceEquals(parduotuve, null))
            {
                return false;
            }

            if (this.GetType() != parduotuve.GetType())
            {
                return false;
            }

            return Pavadinimas == parduotuve.Pavadinimas && Adresas == parduotuve.Adresas;
        }

        public override int GetHashCode()
        {
            return Pavadinimas.GetHashCode() ^ Adresas.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{0}\r\n{1}\r\n{2}", Pavadinimas, Adresas, Telefonas);
        }
    }
}
