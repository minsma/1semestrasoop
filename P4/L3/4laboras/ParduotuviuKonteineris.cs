﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4laboras
{
    class ParduotuviuKonteineris
    {
        private Parduotuve[] Parduotuves;
        public int Kiekis { get; set; }

        public ParduotuviuKonteineris(int size) 
        {
            Parduotuves = new Parduotuve[size];
            Kiekis = 0;
        }

        public void PridetiParduotuve(Parduotuve parduotuve) 
        {
            Parduotuves[Kiekis] = parduotuve;
            Kiekis++;
        }

        public Parduotuve PaimtiParduotuve(int vieta) 
        {
            return Parduotuves[vieta];
        }

        public int PaimtiParduotuvesIndeksa(Parduotuve parduotuve)
        {
            for(int i = 0; i < Kiekis; i++)
            {
                if (Parduotuves[i].Equals(parduotuve))
                {
                    return i;
                }
            }
            return -1;
        }

        public void PasalintiParduotuve(int vieta)
        {
            for (int i = vieta; i < Kiekis; i++) 
            {
                Parduotuves[i] = Parduotuves[i + 1];
            }
            Kiekis--;
        }

        public bool Contains(Parduotuve parduotuve)
        {
            return Parduotuves.Contains(parduotuve);
        }
    }
}
