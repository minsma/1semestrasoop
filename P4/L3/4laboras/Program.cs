﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4laboras
{
    class Program
    {
        const int MaksimalusParduotuviuKiekis = 50;
        const int MaksimalusPrekiukiekis = 200;
        const int AuskaruUzsegimoTipuKiekis = 5;
        const double ZiedoDidziausiasSkersmuo = 24.0;
        const string UnikaliuFailas = "Unikalūs.csv";
        const string PigesniuFailas = "300.csv";
        const string RezultatuFailas = "Rezultatai.csv";
        const string PradiniaiDuomenysFailas = "PradiniaiDuomenys.csv";

        static void Main(string[] args)
        {
            ParduotuviuKonteineris parduotuviuKonteineris;

            PradiniuDuomenuSkaitymas(out parduotuviuKonteineris);

            JuvelyrinisGaminys brangiausiaPreke = BrangiausiaPreke(parduotuviuKonteineris);

            Parduotuve brangiausiasZiedas = BrangiausiasZiedas(parduotuviuKonteineris);
            Parduotuve brangiausiAuskarai = BrangiausiAuskarai(parduotuviuKonteineris);
            Parduotuve brangiausiaGrandinele = BrangiausiaGrandinele(parduotuviuKonteineris);

            ParduotuviuKonteineris unikalusDirbiniai;

            PradiniuDuomenuSkaitymas(out unikalusDirbiniai);

            UnikalusDirbiniai(ref unikalusDirbiniai);
            JeiguParduotuveBePrekiu(unikalusDirbiniai);

            JuvelyriniaiDirbiniaiKonteineris pigesniNei300;

            PigesniNei300(parduotuviuKonteineris, out pigesniNei300);

            PigesniuNei300Rikiavimas(pigesniNei300);

            PradiniuDuomenuSpausdinimas(parduotuviuKonteineris);
            SpausdinimasUnikaliu(unikalusDirbiniai);
            PigesniuNei300Spausdinimas(pigesniNei300);
            LikusiuRezultatuSpausdinimas(brangiausiasZiedas, brangiausiaGrandinele, brangiausiAuskarai, brangiausiaPreke);
        }
        /// <summary>
        /// Nuskaitomi pradiniai duomenys is failu, kurie prasideda zodziu Data
        /// </summary>
        /// <param name="parduotuviuKonteineris">Parduotuviu konteineris</param>
        static void PradiniuDuomenuSkaitymas(out ParduotuviuKonteineris parduotuviuKonteineris)
        {
            parduotuviuKonteineris = new ParduotuviuKonteineris(MaksimalusParduotuviuKiekis);

            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "Data*.csv");

            for (int i = 0; i < filePaths.Length; i++)
            {
                JuvelyriniaiDirbiniaiKonteineris Gaminiai = new JuvelyriniaiDirbiniaiKonteineris(MaksimalusPrekiukiekis);

                using (StreamReader reader = new StreamReader(filePaths[i]))
                {
                    string parduotuvesPavadinimas = reader.ReadLine();
                    string parduotuvesAdresas = reader.ReadLine();
                    string parduotuvesTelefonas = reader.ReadLine();

                    Parduotuve parduotuve = new Parduotuve(parduotuvesPavadinimas, parduotuvesAdresas, parduotuvesTelefonas);

                    int vieta = -1;

                    if (!parduotuviuKonteineris.Contains(parduotuve))
                    {
                        parduotuviuKonteineris.PridetiParduotuve(parduotuve);
                        vieta = parduotuviuKonteineris.Kiekis - 1;
                        parduotuviuKonteineris.PaimtiParduotuve(vieta).JuvelyriniaiDirbiniai = new JuvelyriniaiDirbiniaiKonteineris(MaksimalusPrekiukiekis);
                    }
                    else
                    {
                        vieta = parduotuviuKonteineris.PaimtiParduotuvesIndeksa(parduotuve);
                    }

                    int counter = 0;

                    string line = null;

                    while (null != (line = reader.ReadLine()))
                    {
                        if (counter > 0)
                        {
                            string[] values = line.Split(';');
                            string gamintojas = values[0];
                            string pavadinimas = values[1];
                            string metalas = values[2];
                            double svoris = Convert.ToDouble(values[3]);
                            int praba = Convert.ToInt32(values[4]);
                            double kaina = Convert.ToDouble(values[5]);
                            double papildomas = Convert.ToDouble(values[6]);

                            if (papildomas < AuskaruUzsegimoTipuKiekis)
                            {
                                Auskarai auskarai = new Auskarai(gamintojas, pavadinimas, metalas, svoris, praba, kaina, Convert.ToInt32(papildomas));

                                if (!parduotuviuKonteineris.PaimtiParduotuve(vieta).JuvelyriniaiDirbiniai.Contains(auskarai))
                                {
                                    parduotuviuKonteineris.PaimtiParduotuve(vieta).JuvelyriniaiDirbiniai.PridetiJuvelyrinisGaminys(auskarai);
                                }
                            }
                            else if (papildomas > ZiedoDidziausiasSkersmuo)
                            {
                                Grandinele grandinele = new Grandinele(gamintojas, pavadinimas, metalas, svoris, praba, kaina, papildomas);

                                if (!parduotuviuKonteineris.PaimtiParduotuve(vieta).JuvelyriniaiDirbiniai.Contains(grandinele))
                                {
                                    parduotuviuKonteineris.PaimtiParduotuve(vieta).JuvelyriniaiDirbiniai.PridetiJuvelyrinisGaminys(grandinele);
                                }
                            }
                            else if (papildomas > AuskaruUzsegimoTipuKiekis && papildomas < ZiedoDidziausiasSkersmuo)
                            {
                                Ziedas ziedas = new Ziedas(gamintojas, pavadinimas, metalas, svoris, praba, kaina, papildomas);

                                if (!parduotuviuKonteineris.PaimtiParduotuve(vieta).JuvelyriniaiDirbiniai.Contains(ziedas))
                                {
                                    parduotuviuKonteineris.PaimtiParduotuve(vieta).JuvelyriniaiDirbiniai.PridetiJuvelyrinisGaminys(ziedas);
                                }
                            }
                            else
                            {
                                Console.WriteLine("Blogi pradiniai duomenys.");
                            }
                        }
                        else
                        {
                            counter++;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija surandanti brangiausia preke tarp visu parduotuviu
        /// </summary>
        /// <param name="parduotuviuKonteineris">Parduotuviu konteineris</param>
        /// <returns>Grazinamas brangiausios prekes objektas</returns>
        static JuvelyrinisGaminys BrangiausiaPreke(ParduotuviuKonteineris parduotuviuKonteineris)
        {
            JuvelyrinisGaminys brangiausiaPreke = new Ziedas("", "", "", 0, 0, 0, 0);

            for (int i = 0; i < parduotuviuKonteineris.Kiekis; i++)
            {
                for (int j = 0; j < parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                {
                    if (parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) > brangiausiaPreke)
                    {
                        brangiausiaPreke = parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) as JuvelyrinisGaminys;
                    }
                }
            }
            return brangiausiaPreke;
        }
        /// <summary>
        /// Funkcija surandanti, kurioje parduotuveje galima nusipirkti brangiausia zieda
        /// </summary>
        /// <param name="parduotuviuKonteineris">Parduotuviu konteineris</param>
        /// <returns>Grazinama parduotuves, kurioje galima nusipirkti brangiausia zieda, objektas</returns>
        static Parduotuve BrangiausiasZiedas(ParduotuviuKonteineris parduotuviuKonteineris)
        {
            Ziedas brangiausiasZiedas = new Ziedas("", "", "", 0.0, 0, 0.0, 0.0);
            int indeksas = -1;
            for (int i = 0; i < parduotuviuKonteineris.Kiekis; i++)
            {
                for (int j = 0; j < parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                {
                    if (parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) is Ziedas)
                    {
                        if (parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j).Kaina > brangiausiasZiedas.Kaina)
                        {
                            brangiausiasZiedas = parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) as Ziedas;
                            indeksas = i;
                        }
                    }
                }
            }
            if (indeksas > -1)
                return parduotuviuKonteineris.PaimtiParduotuve(indeksas);
            else
                return null;
        }
        /// <summary>
        /// Funkcija surandanti, kurioje parduotuveje galima nusipirkti brangiausius auskarus
        /// </summary>
        /// <param name="parduotuviuKonteineris">Parduotuviu konteineris</param>
        /// <returns>Grazinama parduotuves, kurioje galima nusipirkti brangiausius auskarus, objektas</returns>
        static Parduotuve BrangiausiAuskarai(ParduotuviuKonteineris parduotuviuKonteineris)
        {
            Auskarai brangiausiAuskarai = new Auskarai("", "", "", 0.0, 0, 0.0, 0);
            int indeksas = -1;
            for (int i = 0; i < parduotuviuKonteineris.Kiekis; i++)
            {
                for (int j = 0; j < parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                {
                    if (parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) is Auskarai)
                    {
                        if (parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j).Kaina > brangiausiAuskarai.Kaina)
                        {
                            brangiausiAuskarai = parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) as Auskarai;
                            indeksas = i;
                        }
                    }
                }
            }
            return parduotuviuKonteineris.PaimtiParduotuve(indeksas);
        }
        /// <summary>
        /// Funkcija surandanti, kurioje parduotuveje galima nusipirkti brangiausia grandinele
        /// </summary>
        /// <param name="parduotuviuKonteineris">Parduotuviu konteineris</param>
        /// <returns>Grazinama parduotuves, kurioje galima nusipirkti brangiausia grandinele, objektas</returns>
        static Parduotuve BrangiausiaGrandinele(ParduotuviuKonteineris parduotuviuKonteineris)
        {
            Grandinele brangiausiaGrandinele = new Grandinele("", "", "", 0.0, 0, 0.0, 0.0);
            int indeksas = -1;
            for (int i = 0; i < parduotuviuKonteineris.Kiekis; i++)
            {
                for (int j = 0; j < parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                {
                    if (parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) is Grandinele)
                    {
                        if (parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j).Kaina > brangiausiaGrandinele.Kaina)
                        {
                            brangiausiaGrandinele = parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) as Grandinele;
                            indeksas = i;
                        }
                    }
                }
            }
            return parduotuviuKonteineris.PaimtiParduotuve(indeksas);
        }
        /// <summary>
        /// Funckija atrenkanti unikalius juvelyrinius dirbininius konteineryje
        /// </summary>
        /// <param name="unikalusDirbiniai">Unikaliu dirbiniu konteineris</param>
        static void UnikalusDirbiniai(ref ParduotuviuKonteineris unikalusDirbiniai)
        {
            for (int i = 0; i < unikalusDirbiniai.Kiekis; i++)
            {
                for (int j = 0; j < unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                {
                    int times = 0;

                    for (int z = i + 1; z < unikalusDirbiniai.Kiekis; z++)
                    {
                        for (int k = 0; k < unikalusDirbiniai.PaimtiParduotuve(z).JuvelyriniaiDirbiniai.Kiekis; k++)
                        {
                            if (unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j) == unikalusDirbiniai.PaimtiParduotuve(z).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(k))
                            {
                                times++;
                                unikalusDirbiniai.PaimtiParduotuve(z).JuvelyriniaiDirbiniai.SalintiJuvelyrinisGaminys(k);
                                k--;
                            }
                        }
                    }
                    if (times > 0)
                    {
                        unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.SalintiJuvelyrinisGaminys(j);
                        j--;
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija tikrinanti ar yra tokia parduotuve
        /// </summary>
        /// <param name="unikalusDirbiniai">Unikaliu gaminiu konteineris</param>
        /// <param name="parduotuve">Ieskoma parduotuve</param>
        /// <returns>Jeigu surandama parduotuve, ji grazinama, jeigu nerandama grazinama null</returns>
        static Parduotuve ArYraTokiaParduotuve(ParduotuviuKonteineris unikalusDirbiniai, Parduotuve parduotuve)
        {
            for (int i = 0; i < unikalusDirbiniai.Kiekis; i++)
            {
                for (int j = 0; j < unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                {
                    if (unikalusDirbiniai.PaimtiParduotuve(i).Equals(parduotuve))
                    {
                        return unikalusDirbiniai.PaimtiParduotuve(i);
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Funkcija tikrinanti ar nera besikartojanciu juvelyriniu dirbiniu
        /// </summary>
        /// <param name="unikalusDirbiniai">Juvelyriniu dirbiniu konteineris</param>
        /// <param name="pavadinimas">Ieskomojo pavadinimas</param>
        /// <returns>Jeigu randamas pasikartojimas grazinama true, jeigu ne grazinama false</returns>
        static bool ArNesikartoja(ParduotuviuKonteineris unikalusDirbiniai, string pavadinimas)
        {
            for (int i = 0; i < unikalusDirbiniai.Kiekis; i++)
            {
                for (int j = 0; j < unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                {
                    if (unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j).Pavadinimas == pavadinimas)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Funkcija tikrinanti ar parduotuve turi prekiu, jeigu neturi pasalinama
        /// </summary>
        /// <param name="unikalusDirbiniai">Unikaliu dirbiniu konteineris</param>
        static void JeiguParduotuveBePrekiu(ParduotuviuKonteineris unikalusDirbiniai)
        {
            for (int i = 0; i < unikalusDirbiniai.Kiekis; i++)
            {
                if (unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis == 0)
                {
                    unikalusDirbiniai.PasalintiParduotuve(i);
                    i--;
                }
            }
        }
        /// <summary>
        /// Funkcija surandanti juvelyrinius dirbinius pigesnius nei 300
        /// </summary>
        /// <param name="parduotuviuKonteineris">Parduotuviu konteineris</param>
        /// <param name="pigesniNei300">Juvelyriniu dirbiniu pigesniu nei 300 konteineris</param>
        static void PigesniNei300(ParduotuviuKonteineris parduotuviuKonteineris, out JuvelyriniaiDirbiniaiKonteineris pigesniNei300)
        {
            pigesniNei300 = new JuvelyriniaiDirbiniaiKonteineris(MaksimalusPrekiukiekis);

            for (int i = 0; i < parduotuviuKonteineris.Kiekis; i++)
            {
                for (int j = 0; j < parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                {
                    if (parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j).Kaina < 300)
                    {
                        pigesniNei300.PridetiJuvelyrinisGaminys(parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j));
                    }
                }
            }
        }
        /// <summary>
        /// Juvelyriniu dirbiniu pigesniu nei 300 rikiavimas
        /// </summary>
        /// <param name="pigesniNei300">Juvelyriniu dirbiniu pigesniu nei 300 konteineris</param>
        static void PigesniuNei300Rikiavimas(JuvelyriniaiDirbiniaiKonteineris pigesniNei300)
        {
            pigesniNei300.Rikiuoti();
        }
        /// <summary>
        /// Pradiniu duomenu atspausdinimas lentele
        /// </summary>
        /// <param name="parduotuviuKonteineris">Parduotuviu konteineris</param>
        static void PradiniuDuomenuSpausdinimas(ParduotuviuKonteineris parduotuviuKonteineris)
        {
            using (StreamWriter writer = new StreamWriter(@PradiniaiDuomenysFailas))
            {
                for (int i = 0; i < parduotuviuKonteineris.Kiekis; i++)
                {
                    writer.WriteLine(parduotuviuKonteineris.PaimtiParduotuve(i));
                    writer.WriteLine("---------------------------------------------------------------------------------------");
                    writer.WriteLine("|{0,-20}|{1,-20}|{2,-15}|{3, -6}|{4,-6}|{5,-6}|{6,-6}|", "Gamintojas", "Pavadinimas", "Metalas", "Svoris", "Praba", "Kaina", "Papild");
                    writer.WriteLine("---------------------------------------------------------------------------------------");
                    for (int j = 0; j < parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                    {
                        writer.WriteLine(parduotuviuKonteineris.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j));
                        writer.WriteLine("---------------------------------------------------------------------------------------");
                    }
                }
            }
        }
        /// <summary>
        /// Funckija spausdinanti unikalius dirbinius i CSV faila
        /// </summary>
        /// <param name="unikalusDirbiniai">Unikaliu dirbiniu konteineris</param>
        static void SpausdinimasUnikaliu(ParduotuviuKonteineris unikalusDirbiniai)
        {
            using (StreamWriter writer = new StreamWriter(@UnikaliuFailas))
            {
                for (int i = 0; i < unikalusDirbiniai.Kiekis; i++)
                {
                    writer.WriteLine("---------------------------------------------------------------------------------------");
                    writer.WriteLine(unikalusDirbiniai.PaimtiParduotuve(i));
                    writer.WriteLine("---------------------------------------------------------------------------------------");
                    writer.WriteLine("|{0,-20}|{1,-20}|{2,-15}|{3, -6}|{4,-6}|{5,-6}|{6,-6}|", "Gamintojas", "Pavadinimas", "Metalas", "Svoris", "Praba", "Kaina", "Papild");
                    writer.WriteLine("---------------------------------------------------------------------------------------");
                    for (int j = 0; j < unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.Kiekis; j++)
                    {
                        writer.WriteLine(unikalusDirbiniai.PaimtiParduotuve(i).JuvelyriniaiDirbiniai.PaimtiJuvelyriniGamini(j));
                        writer.WriteLine("---------------------------------------------------------------------------------------");
                    }
                }
            }
        }
        /// <summary>
        /// Funkcija spausdinanti dirbinius pigesnius nei 300 i CSV faila
        /// </summary>
        /// <param name="pigesniNei300">Juvelyriniu dirbiniu pigesniu nei 300 konteineris</param>
        static void PigesniuNei300Spausdinimas(JuvelyriniaiDirbiniaiKonteineris pigesniNei300)
        {
            using (StreamWriter writer = new StreamWriter(@PigesniuFailas))
            {
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine("|{0,-20}|{1,-20}|{2,-15}|{3, -6}|{4,-6}|{5,-6}|{6,-6}|", "Gamintojas", "Pavadinimas", "Metalas", "Svoris", "Praba", "Kaina", "Papild");
                writer.WriteLine("---------------------------------------------------------------------------------------");
                for (int i = 0; i < pigesniNei300.Kiekis; i++)
                {
                    writer.WriteLine(pigesniNei300.PaimtiJuvelyriniGamini(i));
                    writer.WriteLine("---------------------------------------------------------------------------------------");

                }
            }
        }
        /// <summary>
        /// Funkcija spausdinanti brangiausio juvelyrinio dirbinio, brangiausios grandineles parduotuves, brangiausiu auskaru parduotuves ir brangiausio ziedo parduotuves rezultatus i CSV faila
        /// </summary>
        /// <param name="brangiausiasZiedas">Parduotuve parduodanti brangiausia zieda</param>
        /// <param name="brangiausiaGrandinele">Parduotuve parduodanti brangiausia grandinele</param>
        /// <param name="brangiausiAuskarai">Parduotuve parduodanti brangiausius auskarus</param>
        /// <param name="brangiausiaPreke">Brangiausias juvelyrinis gaminys</param>
        static void LikusiuRezultatuSpausdinimas(Parduotuve brangiausiasZiedas, Parduotuve brangiausiaGrandinele, Parduotuve brangiausiAuskarai, JuvelyrinisGaminys brangiausiaPreke)
        {
            using (StreamWriter writer = new StreamWriter(@RezultatuFailas))
            {
                writer.WriteLine("Brangiausia preke:");
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine("|{0,-20}|{1,-20}|{2,-15}|{3, -6}|{4,-6}|{5,-6}|{6,-6}|", "Gamintojas", "Pavadinimas", "Metalas", "Svoris", "Praba", "Kaina", "Papild");
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine(brangiausiaPreke);
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine("Brangiausia Zieda galima nusipirkti:");
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine(brangiausiasZiedas);
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine("Brangiausia grandinele galima nusipirkti:");
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine(brangiausiaGrandinele);
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine("Brangiausius auskarus galima nusipirkti:");
                writer.WriteLine("---------------------------------------------------------------------------------------");
                writer.WriteLine(brangiausiAuskarai);
                writer.WriteLine("---------------------------------------------------------------------------------------");
            }
        }
    }
}
