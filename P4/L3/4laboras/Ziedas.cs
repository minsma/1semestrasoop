﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4laboras
{
    class Ziedas : JuvelyrinisGaminys
    {
        public double Dydis { get; set; }

        public Ziedas(string gamintojas, string pavadinimas, string metalas, double svoris,
            int praba, double kaina, double dydis)
            : base(gamintojas, pavadinimas, metalas, svoris, praba, kaina)
        {
            Dydis = dydis;
        }

        public override string ToString()
        {
            return String.Format("|{0,-20}|{1,-20}|{2,-15}|{3, 6}|{4,6}|{5,6}|{6,6}|", Gamintojas, Pavadinimas, Metalas, Svoris, Praba, Kaina, Dydis);
        }
    }
}
