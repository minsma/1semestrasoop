﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4._1
{
    class AnimalMarked : Animal
    {
        private static int VaccinationDuration = 1;

        public override bool isVaccinationExpired()
        {
            return VaccinationDate.AddYears(VaccinationDuration).CompareTo(DateTime.Now) > 0;
        }

        public AnimalMarked(string name, int chipId, string breed, string owner, string phone, DateTime vaccinationDate)
            : base(name, chipId, breed, owner, phone, vaccinationDate)
        {
            ChipId = chipId;
        }
    }
}
