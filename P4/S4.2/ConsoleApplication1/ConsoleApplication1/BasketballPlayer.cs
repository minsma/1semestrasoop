﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class BasketballPlayer : Player
    {
        public int ReboundsCount { get; set; }
        public int AssistsCount { get; set; }
        public int PointsMade { get; set; }

        public BasketballPlayer(string teamName, string firstName, string lastName, int playedMatchesCount, int reboundsCount, 
            int assistsCount, int pointsMade) : base(teamName, firstName, lastName, playedMatchesCount)
        {
            ReboundsCount = reboundsCount;
            AssistsCount = assistsCount;
            PointsMade = pointsMade;
        }

        public override string ToString()
        {
            return String.Format("|{0, -20}|{1, -15}|{2, -15}|{3, -3}|{4, -3}|{5, -3}|{6, -3}|", TeamName, FirstName, LastName, 
                PlayedMatchesCount, PointsMade, AssistsCount, ReboundsCount);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as BasketballPlayer);
        }

        public bool Equals(BasketballPlayer basketballPlayer)
        {
            return base.Equals(basketballPlayer);
        }

        public override int GetHashCode()
        {
            return TeamName.GetHashCode() ^ FirstName.GetHashCode() ^ LastName.GetHashCode() ^ PlayedMatchesCount.GetHashCode() ^
                ReboundsCount.GetHashCode() ^ AssistsCount.GetHashCode() ^ PointsMade.GetHashCode();
        }
    }
}
