﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    abstract class Player
    {
        public string TeamName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PlayedMatchesCount { get; set; }

        public Player(string teamName, string firstName, string lastName, int playedMatchesCounter)
        {
            TeamName = teamName;
            FirstName = firstName;
            LastName = lastName;
            PlayedMatchesCount = playedMatchesCounter;
        }
    }
}
