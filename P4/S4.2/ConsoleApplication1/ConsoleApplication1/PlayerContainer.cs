﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class PlayerContainer
    {
        private Player[] Players;
        public int Count { get; set; }

        public PlayerContainer(int size)
        {
            Players = new Player[size];
            Count = 0;
        }
        public void AddPlayer(Player player)
        {
            Players[Count] = player;
            Count++;
        }
        public void SetPlayer(Player player, int place)
        {
            Players[place] = player;
        }
        public Player TakePlayer(int place)
        {
            return Players[place];
        }
        public void RemovePlayer(Player player)
        {
            int i = 0;
            while(i < Count)
            {
                if (Players[i].Equals(player))
                {
                    Count--;
                    for(int j = i; j < Count; j++)
                    {
                        Players[j] = Players[j + 1];
                    }
                    break;
                }
                i++;
            }
        }
        public void RemovePlayer(int place)
        {
            for(int i = place; i < Count; i++)
            {
                Players[i] = Players[i + 1];
            }
            Count--;
        }
        public bool Contains(Player player)
        {
            return Players.Contains(player);
        }
    }
}
