﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        const int MaxTeamsAmount = 10;
        const int MaxPlayersAmount = 50;
        const string TeamData = "Teams.csv";
        const string Results = "Results.csv";

        static void Main(string[] args)
        {
            TeamContainer teamContainer;

            Reading(out teamContainer);

            PlayersSelection(teamContainer);

            PrintResults(teamContainer);

        }
        private static void ReadTeamData(out TeamContainer teamContainer)
        {
            teamContainer = new TeamContainer(MaxTeamsAmount);

            using (StreamReader reader = new StreamReader(TeamData))
            {
                string line = null;
                while (null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(';');
                    string name = values[0];
                    string cityName = values[1];
                    string coach = values[2];
                    int matchsCount = Convert.ToInt32(values[3]);
                    Team team = new Team(name, cityName, coach, matchsCount);
                    teamContainer.AddTeam(team);
                    teamContainer.TakeTeam(teamContainer.Count - 1).PlayerContainer = new PlayerContainer(MaxPlayersAmount);
                }
            }
        }
        private static int GetTeamPosByname(TeamContainer teamContainer, string name)
        {
            for (int i = 0; i < teamContainer.Count; i++)
            {
                if (teamContainer.TakeTeam(i).Name == name)
                {
                    return i;
                }
            }
            return -1;
        }
        private static void ReadPlayersData(ref TeamContainer teamContainer, string filePath)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line = null;
                while (null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(';');
                    int pos = GetTeamPosByname(teamContainer, values[0]);
                    if (pos > -1)
                    {
                        switch (values.Length)
                        {
                            case 8:
                                string teamName = values[0];
                                string firstName = values[1];
                                string lastName = values[2];
                                int playedMatchesCount = Convert.ToInt32(values[3]);
                                int pointsMade = Convert.ToInt32(values[4]);
                                int reboundsCount = Convert.ToInt32(values[5]);
                                int assistsCount = Convert.ToInt32(values[6]);
                                BasketballPlayer basketballPlayer = new BasketballPlayer(teamName, firstName, lastName, playedMatchesCount, reboundsCount, assistsCount, pointsMade);
                                teamContainer.TakeTeam(pos).PlayerContainer.AddPlayer(basketballPlayer);
                                break;
                            case 7:
                                string teamname = values[0];
                                string firstname = values[1];
                                string lastname = values[2];
                                int playedmatchesCount = Convert.ToInt32(values[3]);
                                int goalsMade = Convert.ToInt32(values[4]);
                                int yellowCardsCount = Convert.ToInt32(values[5]);
                                SoccerPlayer soccerPlayer = new SoccerPlayer(teamname, firstname, lastname, playedmatchesCount, yellowCardsCount, goalsMade);
                                teamContainer.TakeTeam(pos).PlayerContainer.AddPlayer(soccerPlayer);
                                break;
                            default:
                                Console.WriteLine("Blogi pradiniai duomenys");
                                break;
                        }
                    }
                }
            }
        }
        private static void Reading(out TeamContainer teamContainer)
        {
            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "Players*.csv");

            ReadTeamData(out teamContainer);

            foreach (string path in filePaths)
            {
                ReadPlayersData(ref teamContainer, path);
            }
        }
        private static void PlayersSelection(TeamContainer teamContainer)
        {
            double assistsAvg = 0, pointsAvg = 0, goalsAvg = 0, yellowCardsAvg = 0;
            for (int i = 0; i < teamContainer.Count; i++)
            {
                if (teamContainer.TakeTeam(i).PlayerContainer.TakePlayer(0) is BasketballPlayer)
                {
                    pointsAvg = BasketballPlayersPointsAvg(teamContainer.TakeTeam(i).PlayerContainer) / teamContainer.TakeTeam(i).PlayedMatchsCount;
                    assistsAvg = BasketballPlayersAssistsAvg(teamContainer.TakeTeam(i).PlayerContainer) / teamContainer.TakeTeam(i).PlayedMatchsCount;
                }
                else if(teamContainer.TakeTeam(i).PlayerContainer.TakePlayer(0) is SoccerPlayer)
                {
                    goalsAvg = SoccerPlayersGoalsAvg(teamContainer.TakeTeam(i).PlayerContainer) / teamContainer.TakeTeam(i).PlayedMatchsCount;
                    yellowCardsAvg = SoccerPlayersYellowCardsAvg(teamContainer.TakeTeam(i).PlayerContainer) / teamContainer.TakeTeam(i).PlayedMatchsCount;
                }
                for (int j = 0; j < teamContainer.TakeTeam(i).PlayerContainer.Count; j++)
                {
                    if (teamContainer.TakeTeam(i).PlayerContainer.TakePlayer(j) is BasketballPlayer)
                    {
                        BasketballPlayer basketballPlayer = teamContainer.TakeTeam(i).PlayerContainer.TakePlayer(j) as BasketballPlayer;

                        if ((double)basketballPlayer.PointsMade / basketballPlayer.PlayedMatchesCount < pointsAvg || basketballPlayer.PlayedMatchesCount != teamContainer.TakeTeam(i).PlayedMatchsCount ||
                            (double)basketballPlayer.AssistsCount / basketballPlayer.PlayedMatchesCount < assistsAvg)
                        {
                            teamContainer.TakeTeam(i).PlayerContainer.RemovePlayer(j);
                            j--;
                        }

                    }
                    else if (teamContainer.TakeTeam(i).PlayerContainer.TakePlayer(j) is SoccerPlayer)
                    {
                        SoccerPlayer soccerPlayer = teamContainer.TakeTeam(i).PlayerContainer.TakePlayer(j) as SoccerPlayer;

                        if ((double)soccerPlayer.GoalsCount / soccerPlayer.PlayedMatchesCount < goalsAvg || soccerPlayer.PlayedMatchesCount != teamContainer.TakeTeam(i).PlayedMatchsCount ||
                           (double) soccerPlayer.YellowCardsCount / soccerPlayer.PlayedMatchesCount > yellowCardsAvg)
                        {
                            teamContainer.TakeTeam(i).PlayerContainer.RemovePlayer(j);
                            j--;
                        }
                    }
                }
            }
        }
        private static double BasketballPlayersPointsAvg(PlayerContainer basketballPlayers)
        {
            double sum = 0;
            for (int i = 0; i < basketballPlayers.Count; i++)
            {
                BasketballPlayer basketballPlayer = basketballPlayers.TakePlayer(i) as BasketballPlayer;
                sum += basketballPlayer.PointsMade;
            }
            return (double)sum / basketballPlayers.Count;
        }
        private static double SoccerPlayersGoalsAvg(PlayerContainer soccerPlayers)
        {
            double sum = 0;
            for (int i = 0; i < soccerPlayers.Count; i++)
            {
                SoccerPlayer soccerPlayer = soccerPlayers.TakePlayer(i) as SoccerPlayer;
                sum += soccerPlayer.GoalsCount;
            }
            return (double)sum / soccerPlayers.Count;
        }
        private static double BasketballPlayersAssistsAvg(PlayerContainer basketballPlayers)
        {
            double sum = 0;
            for (int i = 0; i < basketballPlayers.Count; i++)
            {
                BasketballPlayer basketballPlayer = basketballPlayers.TakePlayer(i) as BasketballPlayer;
                sum += basketballPlayer.AssistsCount;
            }
            return (double)sum / basketballPlayers.Count;
        }
        private static double SoccerPlayersYellowCardsAvg(PlayerContainer soccerPlayers)
        {
            double sum = 0;
            for (int i = 0; i < soccerPlayers.Count; i++)
            {
                SoccerPlayer soccerPlayer = soccerPlayers.TakePlayer(i) as SoccerPlayer;
                sum += soccerPlayer.YellowCardsCount;
            }
            return (double)sum / soccerPlayers.Count;
        }
        private static void PrintResults(TeamContainer teamContainer)
        {
            using (StreamWriter writer = new StreamWriter(Results))
            {
                for(int i = 0; i < teamContainer.Count; i++)
                {
                    writer.WriteLine(teamContainer.TakeTeam(i));
                    for(int j = 0; j< teamContainer.TakeTeam(i).PlayerContainer.Count; j++)
                    {
                        writer.WriteLine(teamContainer.TakeTeam(i).PlayerContainer.TakePlayer(j));
                    }
                    writer.WriteLine("");
                }
            }
        }
    }
}
