﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class SoccerPlayer : Player
    {
        public int YellowCardsCount { get; set;}
        public int GoalsCount { get; set; }

        public SoccerPlayer(string teamName, string firstName, string lastName, int playedMatchesCounter, int yellowCardsCount,
            int goalsCount) : base(teamName, firstName, lastName, playedMatchesCounter)
        {
            GoalsCount = goalsCount;
            YellowCardsCount = yellowCardsCount;
        }

        public override string ToString()
        {
            return String.Format("|{0, -20}|{1, -15}|{2, -15}|{3, -3}|{4, -3}|{5, -3}|", TeamName, FirstName, LastName, PlayedMatchesCount, GoalsCount, YellowCardsCount);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as SoccerPlayer);
        }

        public bool Equals(SoccerPlayer soccerPlayer)
        {
            return base.Equals(soccerPlayer);
        }

        public override int GetHashCode()
        {
            return TeamName.GetHashCode() ^ FirstName.GetHashCode() ^ LastName.GetHashCode() ^ PlayedMatchesCount.GetHashCode() ^ 
                YellowCardsCount.GetHashCode() ^ GoalsCount.GetHashCode();
         }
    }
}
