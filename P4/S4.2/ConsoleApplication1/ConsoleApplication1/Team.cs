﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Team
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string Coach { get; set; }
        public int PlayedMatchsCount { get; set; }
        public PlayerContainer PlayerContainer { get; set; }

        public Team(string name, string city, string coach, int playedMatchsCount)
        {
            Name = name;
            City = city;
            Coach = coach;
            PlayedMatchsCount = playedMatchsCount;
        }

        public override string ToString()
        {
            return String.Format("{0, -20}\r\n{1, -20}\r\n{2, -20}\r\n{3, 3}", Name, City, Coach, PlayedMatchsCount);
        }
    }
}