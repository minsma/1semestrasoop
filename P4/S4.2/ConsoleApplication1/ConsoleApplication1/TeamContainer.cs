﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class TeamContainer
    {
        private Team[] Teams;
        public int Count;

        public TeamContainer(int size)
        {
            Teams = new Team[size];
            Count = 0;
        }

        public void AddTeam(Team team)
        {
            Teams[Count] = team;
            Count++;
        }

        public Team TakeTeam(int place)
        {
            return Teams[place];
        }
    }
}
