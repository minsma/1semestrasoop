﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_uzduotis
{
    class Program
    {
        static void Main(string[] args)
        {
            const string CFd = "..\\..\\Duom.txt";
            const string CFr = "..\\..\\Rezultatai.txt";
            const string CFa = "..\\..\\Analize.txt";
            Apdoroti(CFd, CFr, CFa);
        }
        static void Apdoroti(string fv, string fvr, string fa)
        {
            string[] lines = File.ReadAllLines(fv, Encoding.GetEncoding(1257));
            bool arRastasKomentaras = false;

            using (var fr = File.CreateText(fvr))
            {
                using (var far = File.CreateText(fa))
                {
                    bool arRastasKomentarasSuZvaigzde = false;

                    for (int i = 0; i < lines.Length; i++)
                    {
                        string nauja = lines[i];

                        if (ArRastaKomentaroSuZvaigzdePradzia(lines[i]) == true && ArRastaKomentaroSuZvaigzdePabaiga(i, lines) == true)
                        {
                            arRastasKomentarasSuZvaigzde = true;
                            nauja = nauja.Remove(KomentaroSuZvaigzdePradzia(lines[i]));
                            if (nauja.Length > 0)
                            {
                                fr.WriteLine(nauja);
                            }
                            far.WriteLine(lines[i]);
                        }
                        else if (arRastasKomentarasSuZvaigzde == true && KomentaroSuZvaigzdePabaiga(lines[i]) == -1 && ArRastaKomentaroSuZvaigzdePabaiga(i, lines) == true)
                        {
                            nauja = nauja.Remove(0);

                            far.WriteLine(lines[i]);
                        }
                        else if (arRastasKomentarasSuZvaigzde == true && KomentaroSuZvaigzdePabaiga(lines[i]) > -1 && ArRastaKomentaroSuZvaigzdePabaiga(i, lines) == true)
                        {
                            arRastasKomentarasSuZvaigzde = false;
                            nauja = nauja.Remove(0, KomentaroSuZvaigzdePabaiga(lines[i]));

                            if (nauja.Length > 0)
                            {
                                fr.WriteLine(nauja);
                            }

                            far.WriteLine(lines[i]);
                        }
                        else if (BeKomentaru(lines[i], out nauja))
                        {
                            if (nauja.Length > 0)
                            {
                                fr.WriteLine(nauja);
                            }
                            far.WriteLine(lines[i]);
                        }
                        else
                        {
                            fr.WriteLine(lines[i]);
                        }
                    }
                }
            }
        }
        static bool BeKomentaru(string line, out string nauja)
        {
            nauja = line;
            for (int i = 0; i < line.Length - 1; i++)
            {
                if (line[i] == '/' && line[i + 1] == '/')
                {
                    nauja = line.Remove(i);
                    return true;
                }
            }
            return false;
        }
        static bool ArRastaKomentaroSuZvaigzdePradzia(string line)
        {
            for (int i = 0; i < line.Length - 1; i++)
            {
                if (line[i] == '/' && line[i + 1] == '*')
                {
                    return true;
                }
            }
            return false;
        }
        static bool ArRastaKomentaroSuZvaigzdePabaiga(int vieta, string[] lines)
        {
            for (int i = vieta; i < lines.Length; i++)
            {
                for(int j = 0; j < lines[i].Length - 1; j++)
                {
                    if(lines[i][j] == '*' && lines[i][j+1] == '/')
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        static int KomentaroSuZvaigzdePradzia(string line)
        {
            int vieta = -1;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == '/' && line[i + 1] == '*')
                {
                    vieta = i;
                }
            }
            return vieta;
        }
        static int KomentaroSuZvaigzdePabaiga(string line)
        {
            int vieta = -1;
            for (int i = 0; i < line.Length-1; i++)
            {
                if (line[i] == '*' && line[i + 1] == '/')
                {
                    vieta = i + 1;
                }
            }
            return vieta;
        }
    }
}

