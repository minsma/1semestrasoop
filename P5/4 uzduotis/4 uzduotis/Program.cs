﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_uzduotis
{
    class Program
    {
        static void Main(string[] args)
        {
            const string CFd = "..\\..\\Duomenys.txt";
            const string SalinamiZodziai = "..\\..\\SalinamiZodziai.txt";
            const string CFr = "..\\..\\Rezultatai.txt";
            string skyr = " .,!?:;()\t'";
            Apdoroti(CFd, CFr, SalinamiZodziai, skyr);
        }
        static void Apdoroti(string fd, string fr, string SalinamiZodziaiFile, string skyrikliai)
        {
            string[] lines = File.ReadAllLines(fd, Encoding.GetEncoding(1257));
            string[] zodziai = File.ReadAllLines(SalinamiZodziaiFile, Encoding.GetEncoding(1257));

            using (var far = File.CreateText(fr))
            {
                for (int j = 0; j < lines.Length; j++)
                {
                    for (int i = 0; i < lines.Length; i++)
                    {
                        int vieta;
                        string zodis;
                        char skyriklis;

                        DuomenuSuradimasSalinimui(lines[i], skyrikliai, zodziai, out vieta, out skyriklis, out zodis);

                        if (vieta > -1)
                        {
                            lines[i] = lines[i].Remove(vieta, zodis.Length + 1);
                            i--;
                        }
                    }
                    far.WriteLine(lines[j]);
                }
            }
        }
        static void DuomenuSuradimasSalinimui(string line, string skyrikliai, string[] zodziai, out int vieta, out char skyriklis, out string zodis)
        {
            vieta = -1;
            skyriklis = '\0';
            zodis = null;
            for (int i = 0; i < zodziai.Length; i++)
            {
                for (int j = 0; j < skyrikliai.Length; j++)
                {
                    if (line.IndexOf(zodziai[i] + skyrikliai[j]) > -1)
                    {
                        vieta = line.IndexOf(zodziai[i] + skyrikliai[j]);
                        zodis = zodziai[i];
                        skyriklis = skyrikliai[j];
                    }
                }
            }
        }
    }
}
