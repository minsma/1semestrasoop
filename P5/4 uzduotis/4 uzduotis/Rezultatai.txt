(g. 1964 m. gruodžio 19 d. Kaune) – Lietuvos 
olimpinis ir pasaulio čempionas  nuo Arvydas
2011 m. spalio 24 d. Lietuvos krepšinio federacijos prezidentas.
Profesionalaus žaidėjo karjerą pradėjo 1981 m.
Kauno krepšinio klube "Žalgiris".
tris sezonus iš eilės (1985–1987 m.). Arvydas
padėjo komandai iškovoti SSRS krepšinio čempionato aukso medalius.
1982 m. SSRS rinktinės sudėtyje Arvydas
dalyvavo pasaulio krepšinio čempionate ir laimėjo auksą.
