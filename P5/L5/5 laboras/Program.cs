﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace _5_laboras
{
    class Program
    {
        public const int MaksimalusZodziuKiekis = 500;
        const string Knyga1 = "..//..//Knyga1.txt";
        const string Knyga2 = "..//..//Knyga2.txt";
        const string Analize = "..//..//Analize.txt";
        const string ManoKnyga = "..//..//ManoKnyga.txt";

        static void Main(string[] args)
        {
            ZodziuKonteineris knyga1;
            ZodziuKonteineris knyga2;

            SkaitytiTekstaIKlase(Knyga1, out knyga1);

            string[] operands = Regex.Split(knyga1.Tekstas, ".");

            foreach(string oper in operands)
            {
                Console.WriteLine(oper);
            }

        }
        static void SkaitytiTekstaIKlase(string fr, out ZodziuKonteineris zodziuKonteineris)
        {
            zodziuKonteineris = new ZodziuKonteineris(File.ReadAllText(fr));
        }
        static void TekstoSkaidymasIZodzius(ref ZodziuKonteineris zodziuKonteineris, string isolations)
        {
            string[] zodziai = Regex.Split(zodziuKonteineris.Tekstas, isolations);

            foreach(string zodz in zodziai)
            {
                Zodis zodis = new Zodis(zodz, 0);
                zodziuKonteineris.PridetiZodi(zodis);
            }
        }
    }
}
