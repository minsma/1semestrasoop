﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_laboras
{
    class Zodis
    {
        public string Zodis_ { get; set; }
        public int Pasikartojimas { get; set; }

        public Zodis(string zodis_, int pasikartojimas)
        {
            Zodis_ = zodis_;
            Pasikartojimas = pasikartojimas;
        }

        public override string ToString()
        {
            return string.Format("{0, -20} {1, 4}", Zodis_, Pasikartojimas);
        }
    }
}
