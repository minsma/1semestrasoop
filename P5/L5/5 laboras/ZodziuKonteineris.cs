﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_laboras
{
    class ZodziuKonteineris
    {
        private Zodis[] Zodziai;
        private Zodis[][] Sakinys;
        public int ZodziuKiekis { get; set; }
        public string Tekstas { get; set; }

        public ZodziuKonteineris(string tekstas)
        {
            Tekstas = tekstas;
            Zodziai = new Zodis[Program.MaksimalusZodziuKiekis];
            ZodziuKiekis = 0;
        }

        public void PridetiZodi(Zodis zodis)
        {
            Zodziai[ZodziuKiekis++] = zodis;
        }

        public Zodis PaimtiZodi(int index)
        {
            return Zodziai[index];
        }

        public void SalintiZodi(Zodis zodis)
        {
            for(int i = 0; i < ZodziuKiekis; i++)
            {
                if(Zodziai[i] == zodis)
                {
                    for(int j = i; j < ZodziuKiekis; j++)
                    {
                        Zodziai[j] = Zodziai[j + 1];
                    }
                    ZodziuKiekis--;
                }
            }
        }

        public void SalintiZodiPagalIndeksa(int indeksas)
        {
            for(int i = indeksas; i < ZodziuKiekis; i++)
            {
                Zodziai[i] = Zodziai[i + 1];
            }
            ZodziuKiekis--;
        }
    }
}
